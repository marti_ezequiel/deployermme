﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using ReportingTool.Business.Entities;
using ReportingTool.Struct.Dependency;

namespace ReportingTool.View.Controls
{
    public abstract partial class CheckboxListControl : UserControl
    {
        public CheckboxListControl()
        {
            InitializeComponent();
        }
        
        protected virtual void OnMouseDoubleClick(Object sender, MouseButtonEventArgs e)
        {

        }

        protected virtual void OnChecked(Object sender, RoutedEventArgs e)
        {

        }
    }

    public abstract class CheckboxListControl<T> : CheckboxListControl
        where T : IHasName
    {
        private CheckboxListViewModel<T> ViewModel
        {
            get
            {
                return (CheckboxListViewModel<T>)this.Container.DataContext;
            }
        }

        #region Dependency Properties

        protected abstract Func<T, Boolean> DefaultCheckState { get; }

        public IEnumerable<T> ItemsSource
        {
            get { return (IEnumerable<T>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable<T>), typeof(CheckboxListControl<T>), new PropertyMetadata(new List<T>(), OnEntityListChange));

        public ObservableCollection<T> SelectedEntities
        {
            get { return (ObservableCollection<T>)GetValue(SelectedEntitiesProperty); }
            set { SetValue(SelectedEntitiesProperty, value); }
        }

        public static readonly DependencyProperty SelectedEntitiesProperty =
            DependencyProperty.Register("SelectedEntities", typeof(ObservableCollection<T>), typeof(CheckboxListControl<T>), new PropertyMetadata(new ObservableCollection<T>()));

        #endregion

        #region Constructor

        public CheckboxListControl() : base()
        {
            this.Container.DataContext = DependencyResolver.Instance.Resolve<CheckboxListViewModel<T>>();
        }

        #endregion

        #region Events

        private static void OnEntityListChange(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            CheckboxListControl<T> checkboxList = (CheckboxListControl<T>)dependencyObject;

            if (checkboxList.ItemsSource != null)
            {
                checkboxList.listBox.ItemsSource = new ObservableCollection<CheckboxListItemViewModel<T>>(checkboxList.ItemsSource.Select(m => new CheckboxListItemViewModel<T>(m, checkboxList.DefaultCheckState.Invoke(m))));

                checkboxList.SelectedEntities = new ObservableCollection<T>(checkboxList.listBox.Items.Cast<CheckboxListItemViewModel<T>>().Where(item => item.IsChecked).Select(item => item.Element));
            }
        }
        
        protected override void OnMouseDoubleClick(Object sender, MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));

            if (r.VisualHit.GetType() == typeof(ScrollViewer))
            {
                listBox.UnselectAll();

                return;
            }

            this.ViewModel.ToggleItems(this.listBox.SelectedItems.Cast<CheckboxListItemViewModel<T>>());
        }

        protected override void OnChecked(Object sender, RoutedEventArgs e)
        {
            this.SelectedEntities = new ObservableCollection<T>(this.listBox.Items.Cast<CheckboxListItemViewModel<T>>().Where(item => item.IsChecked).Select(item => item.Element));
        }

        #endregion
    }
}
