﻿using System;
using ReportingTool.Business.Entities;
using ReportingTool.Struct.View;

namespace ReportingTool.View.Controls
{
    public class CheckboxListItemViewModel<T> : ViewModel where T : IHasName
    {
        private Boolean isChecked;

        public CheckboxListItemViewModel()
        {
        }

        public T Element { get; set; }

        public Boolean IsChecked
        {
            get
            {
                return this.isChecked;
            }
            set
            {
                this.isChecked = value;

                this.RaisePropertyChanged("IsChecked");
            }
        }

        public CheckboxListItemViewModel(T element, Boolean isChecked)
        {
            this.isChecked = isChecked;

            this.Element = element;
        }
    }
}
