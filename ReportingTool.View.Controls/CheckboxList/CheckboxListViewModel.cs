﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using ReportingTool.Business.Entities;
using ReportingTool.Struct.View;

namespace ReportingTool.View.Controls
{
    public class CheckboxListViewModel<T> : ViewModel
        where T : IHasName
    {
        public ICommand SelectItemCommand 
        {
            get;
            private set;
        }

        public ICommand SelectAll
        {
            get;
            private set;
        }

        public ICommand UnselectAll
        {
            get;
            private set;
        }

        public CheckboxListViewModel()
        {

        }

        protected override void AddCommands()
        {
            this.SelectItemCommand = new Command<IList>(t => this.ToggleItems(t.Cast<CheckboxListItemViewModel<T>>()));

            this.SelectAll = new Command<IList>(t => this.ToggleItemsTo(t.Cast<CheckboxListItemViewModel<T>>(), true));

            this.UnselectAll = new Command<IList>(t => this.ToggleItemsTo(t.Cast<CheckboxListItemViewModel<T>>(), false));
        }

        internal void ToggleItems(IEnumerable<CheckboxListItemViewModel<T>> selectedItems)
        {
            Boolean result = !selectedItems.All(i => i.IsChecked);

            this.ToggleItemsTo(selectedItems, result);
        }

        private void ToggleItemsTo(IEnumerable<CheckboxListItemViewModel<T>> items, Boolean check)
        {
            foreach (CheckboxListItemViewModel<T> item in items)
            {
                item.IsChecked = check;
            }
        }
    }
}
