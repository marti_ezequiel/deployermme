﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using ReportingTool.Struct.View;

namespace ReportingTool.View.Controls
{
    public abstract class ComboBoxControl<E, T> : ComboBox
        where T : IPresenter
    {
        private IPresenter presenter;

        protected IEnumerable<E> EntityList
        {
            get
            {
                return this.ItemsSource as IEnumerable<E>;
            }
        }

        protected T Presenter
        {
            get
            {
                return (T)this.presenter;
            }
            set
            {
                this.presenter = value;
            }
        }

        public ComboBoxControl(String valueMember, String displayMember)
        {
            this.InitializatePresenter();

            this.Refresh();

            this.SelectedValuePath = valueMember;

            this.DisplayMemberPath = displayMember;
        }

        public virtual void Refresh()
        {
            this.ItemsSource = this.GetEntityList();
        }

        protected abstract IEnumerable<E> GetEntityList();

        protected abstract void InitializatePresenter();
    }
}
