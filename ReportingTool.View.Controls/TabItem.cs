﻿using System;
using MahApps.Metro.Controls;
using ReportingTool.Struct.View;

namespace ReportingTool.View.Controls
{
    public abstract class TabItem : MetroTabItem
    {
        internal ViewModel vm;

        public TabItem(ViewModel vm)
            : base()
        {
            this.vm = vm;

            this.DataContext = this.vm;

            ControlsHelper.SetHeaderFontSize(this, 15);

            this.Style = this.FindResource("MainTabItem") as System.Windows.Style;
        }

        public TabItem(ViewModel vm, String name)
            : this(vm)
        {
            this.Name = name;
        }
    }

    public abstract class TabItem<T> : TabItem
        where T : ViewModel
    {
        public T ViewModel
        {
            get
            {
                return this.vm as T;
            }
        }

        public TabItem(T vm) : base(vm)
        {

        }

        public TabItem(ViewModel vm, String name)
            : base(vm, name)
        {
        }
    }
}
