﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Resources.Parameters;

namespace ReportingTool.View.Controls
{
    public class ReportCheckListControl : CheckboxListControl<RtReport>
    {
        protected override Func<RtReport, Boolean> DefaultCheckState
        {
            get
            {
                return r =>
                    !ignoredFiles.Contains(r.FileName) && 
                    !ignoredFiles.Contains(r.Name) &&
                    !ignoredFiles.Contains(r.FileNameWithoutExtention);
            }
        }

        public ReportCheckListControl()
        {
            this.ignoredFiles = Settings.ExcludedFiles;
        }

        private readonly IEnumerable<String> ignoredFiles;
    }
}
