﻿using System;
using System.Security;
using System.Windows.Input;
using MahApps.Metro.Controls;
using ReportingTool.Business.Entities;

namespace ReportingTool.View.Controls
{
    public partial class CredentialDialogView : MetroWindow, IHavePassword, IDisposable
    {
        CredentialDialogVM VM
        {
            get
            {
                return (CredentialDialogVM)this.DataContext;
            }
        }

        public String User { get { return this.VM.UserName; } }

        public SecureString Password { get { return this.txtPassword.SecurePassword; } }

        public String Domain { get { return this.VM.Domain; } }

        public CredentialDialogView(String url)
        {
            InitializeComponent();

            this.DataContext = new CredentialDialogVM(url);

            FocusManager.SetFocusedElement(this, this.txtPassword);
        }

        public void Dispose()
        {
            this.Close();
        }
    }
}
