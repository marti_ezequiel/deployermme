﻿using System;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Interfaces.Logic;

namespace ReportingTool.View.Controls
{
    public class CredentialController : ICredentialController
    {
        IConfigurationLogic configurationLogic;

        public CredentialController(IConfigurationLogic configurationLogic)
        {
            this.configurationLogic = configurationLogic;
        }

        public CredentialEntity GetCredentials(String serverUrl)
        {
            CredentialEntity credential = this.configurationLogic.GetCredentials();

            if (credential == null)
            {
                using (CredentialDialogView credentialPrompt = new CredentialDialogView(serverUrl))
                {
                    if (!credentialPrompt.ShowDialog() ?? true) return default(CredentialEntity);

                    credential = new CredentialEntity(credentialPrompt.User, credentialPrompt.Password, credentialPrompt.Domain);

                    this.configurationLogic.SetCredentials(credential);
                }
            }

            return credential;
        }

        public void ClearCredentials()
        {
            this.configurationLogic.ClearCredentials();
        }
    }
}
