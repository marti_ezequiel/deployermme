﻿using System;
using System.Text.RegularExpressions;
using System.Windows.Input;
using ReportingTool.Resources.Globalization;
using ReportingTool.Struct.View;

namespace ReportingTool.View.Controls
{
    public class CredentialDialogVM : ViewModel
    {
        #region Properties

        private String url;
        private readonly String UrlPropertyName = nameof(Url);
        public String Url
        {
            get
            {
                return this.url;
            }
            private set
            {
                if (value != url)
                {
                    this.url = value;

                    this.RaisePropertyChanged(UrlPropertyName);
                }
            }
        }

        private String userName;
        private readonly String UserNamePropertyName = nameof(UserName);
        public String UserName
        {
            get
            {
                return this.userName;
            }
            set
            {
                if (value != userName)
                {
                    this.userName = value;

                    if (this.AutocheckDomain) this.CheckForDomain(value);

                    this.RaisePropertyChanged(UserNamePropertyName);
                }
            }
        }

        private String domain;
        private readonly String DomainPropertyName = nameof(Domain);
        public String Domain
        {
            get
            {
                return this.domain;
            }
            set
            {
                if (value != domain)
                {
                    this.domain = value;
                    
                    if (this.AutocheckDomain) this.CheckForDomain(value);

                    this.RaisePropertyChanged(DomainPropertyName);
                }
            }
        }

        private Boolean autocheckDomain;
        private readonly String AutocheckDomainPropertyName = nameof(AutocheckDomain);
        public Boolean AutocheckDomain
        {
            get
            {
                return this.autocheckDomain;
            }
            set
            {
                if (value != autocheckDomain)
                {
                    this.autocheckDomain = value;

                    this.RaisePropertyChanged(AutocheckDomainPropertyName);
                }
            }
        }
        
        public ICommand ButtonOkCommand
        {
            get;
            private set;
        }
        
        public ICommand ButtonCancelCommand
        {
            get;
            private set;
        }
        
        public ICommand TextInputDomainUserNameCommand
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        public CredentialDialogVM(String url)
        {
            this.UserName = Environment.UserName;

            this.Domain = Environment.UserDomainName;

            this.AutocheckDomain = true;

            this.Url = url;
        }

        #endregion

        #region Methods

        protected override void AddCommands()
        {
            base.AddCommands();

            this.ButtonOkCommand = new RelayCommand((s) => SelectOk(s));

            this.ButtonCancelCommand = new RelayCommand((s) => Close(s, false));

            this.TextInputDomainUserNameCommand = new Command<String>((s) => CheckForDomain(s));
        }

        private void SelectOk(Object window)
        {
            if (this.IsEmpty())
            {
                this.ShowError(Messages.UserNamePasswordOrUserDomainIsEmpty);

                return;
            }

            this.Close(window, true);
        }

        private void Close(Object window, Boolean dialogResult)
        {
            CredentialDialogView dialog = (CredentialDialogView)window;

            dialog.DialogResult = dialogResult;

            dialog.Dispose();
        }
        
        private void CheckForDomain(String domainUserName)
        {
            var regex = new Regex(@"[a-z1-9\.\-_]+(\\|\/)[a-z1-9\.\-_]+", RegexOptions.IgnoreCase);

            var match = regex.Match(domainUserName);

            if (match.Success)
            {
                this.UserName = match.Value.Split(match.Groups[1].Value[0])[1];

                this.Domain = match.Value.Split(match.Groups[1].Value[0])[0];
            }
        }

        private Boolean IsEmpty()
        {
            return
                String.IsNullOrWhiteSpace(this.UserName) ||
                String.IsNullOrWhiteSpace(this.Domain);
        }

        #endregion
    }
}
