﻿using System;
using ReportingTool.Business.Entities;

namespace ReportingTool.View.Controls
{
    public interface ICredentialController
    {
        CredentialEntity GetCredentials(String serverUrl);

        void ClearCredentials();
    }
}