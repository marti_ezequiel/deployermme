﻿using System.Windows;
using Control = System.Windows.Controls;

namespace ReportingTool.View.Controls
{
    public class GroupBox : Control.GroupBox
    {
        public GroupBox()
        {
            this.Style = default(Style);
        }
    }
}
