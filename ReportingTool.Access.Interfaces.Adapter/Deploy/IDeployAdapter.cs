﻿using System;
using System.Collections.Generic;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Entities.Deploy;

namespace ReportingTool.Access.Adapter.Interfaces
{
    public interface IDeployAdapter
    {
        void CheckCredentials(CredentialEntity credential);

        Boolean RenderReport(RtReport report, DeployConfigurationModel group, CredentialEntity credential);

        IEnumerable<RtReport> UploadReport(DeployConfigurationModel group, IEnumerable<RtReport> reports, CredentialEntity credential, IProgress<UploadStatus> logUpload, IProgress<WarningStatus> logWarning, IProgress<DataSourceStatus> logDataSource);
    }
}
