﻿using ReportingTool.Business.Entities;

namespace ReportingTool.Access.Adapter.Interfaces
{
    public interface IConfigurationAdapter
    {
        CredentialEntity GetCredentials();

        void SetCredentials(CredentialEntity credentials);

        void ClearCredentials();
    }
}
