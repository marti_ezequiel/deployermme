﻿using System;
using System.IO;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace ReportingTool.Struct.View
{
    public class BrowserDialogPromptControl : IBrowserDialogPromptControl
    {
        public void SaveFile(String fileName, String file, Boolean restoreDirectory = true)
        {
            var saveFileDialog = new CommonSaveFileDialog()
            {
                DefaultFileName = fileName,
                RestoreDirectory = restoreDirectory
            };

            if (saveFileDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                File.WriteAllText(saveFileDialog.FileName, file);
            }
        }

        public String SelectPath(String defaultName, String defaultExtencion, String initialDirectory, Boolean isFolder = false)
        {
            var openFileDialog = new CommonOpenFileDialog()
            {
                InitialDirectory = initialDirectory,
                IsFolderPicker = isFolder
            };

            if (!isFolder) openFileDialog.Filters.Add(new CommonFileDialogFilter(defaultName, defaultExtencion));

            if (openFileDialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                return openFileDialog.FileName;
            }

            return String.Empty;
        }
    }
}
