﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;

namespace ReportingTool.Struct.View
{
    public class TextBoxBehaviour
    {
        static readonly Dictionary<TextBox, Capture> associations = new Dictionary<TextBox, Capture>();

        public static Boolean GetScrollOnTextChanged(DependencyObject dependencyObject)
        {
            return (Boolean)dependencyObject.GetValue(ScrollOnTextChangedProperty);
        }

        public static void SetScrollOnTextChanged(DependencyObject dependencyObject, Boolean value)
        {
            dependencyObject.SetValue(ScrollOnTextChangedProperty, value);
        }

        public static readonly DependencyProperty ScrollOnTextChangedProperty =
            DependencyProperty.RegisterAttached("ScrollOnTextChanged", typeof(Boolean), typeof(TextBoxBehaviour), new UIPropertyMetadata(false, OnScrollOnTextChanged));

        static void OnScrollOnTextChanged(DependencyObject dependencyObject, DependencyPropertyChangedEventArgs e)
        {
            var textBox = dependencyObject as TextBox;
            if (textBox == null)
            {
                return;
            }
            Boolean oldValue = (Boolean)e.OldValue, newValue = (Boolean)e.NewValue;
            if (newValue == oldValue)
            {
                return;
            }
            if (newValue)
            {
                textBox.Loaded += TextBoxLoaded;
                textBox.Unloaded += TextBoxUnloaded;
            }
            else
            {
                textBox.Loaded -= TextBoxLoaded;
                textBox.Unloaded -= TextBoxUnloaded;
                if (associations.ContainsKey(textBox))
                {
                    associations[textBox].Dispose();
                }
            }
        }

        static void TextBoxUnloaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            var textBox = (TextBox)sender;
            associations[textBox].Dispose();
            textBox.Unloaded -= TextBoxUnloaded;
        }

        static void TextBoxLoaded(Object sender, RoutedEventArgs routedEventArgs)
        {
            var textBox = (TextBox)sender;
            textBox.Loaded -= TextBoxLoaded;
            associations[textBox] = new Capture(textBox);
        }

        internal class Capture : IDisposable
        {
            private TextBox TextBox { get; set; }

            public Capture(TextBox textBox)
            {
                TextBox = textBox;
                TextBox.TextChanged += OnTextBoxOnTextChanged;
            }

            private void OnTextBoxOnTextChanged(Object sender, TextChangedEventArgs args)
            {
                TextBox.ScrollToEnd();
            }

            public void Dispose()
            {
                TextBox.TextChanged -= OnTextBoxOnTextChanged;
            }
        }
    }
}
