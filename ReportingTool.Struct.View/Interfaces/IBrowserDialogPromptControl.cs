﻿using System;

namespace ReportingTool.Struct.View
{
    public interface IBrowserDialogPromptControl
    {
        void SaveFile(String fileName,  String file, Boolean restoreDirectory = true);

        String SelectPath(String defaultName, String defaultExtencion, String initialDirectory, Boolean isFolder = false);
    }
}
