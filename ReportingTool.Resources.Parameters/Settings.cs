﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ReportingTool.Resources.Parameters
{
    public static class Settings
    {
        public static String ConfigXmlFilePath
        {
            get
            {
                return LocalSettings.Default.ConfigXmlFilePath;
            }
            set
            {
                LocalSettings.Default.ConfigXmlFilePath = value;

                LocalSettings.Default.Save();
            }
        }

        public static String DefaultEnvironments
        {
            get
            {
                return JObject.Parse(System.IO.File.ReadAllText("config.json"))["environments"].ToString();
            }
        }

        public static IEnumerable<String> ExcludedFiles
        {
            get
            {
                return JsonConvert.DeserializeObject<String[]>(
                    JObject.Parse(System.IO.File.ReadAllText("config.json"))["excludedFiles"].ToString()
                    );
            }
        }

        public static Dictionary<String, String> SubFolderReports
        {
            get
            {
                return JsonConvert.DeserializeObject<Dictionary<String, String>>(
                    JObject.Parse(System.IO.File.ReadAllText("config.json"))["reportSubfolder"].ToString()
                    );
            }
        }
    }
}
