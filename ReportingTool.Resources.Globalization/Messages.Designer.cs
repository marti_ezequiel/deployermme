﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ReportingTool.Resources.Globalization {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Messages {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Messages() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ReportingTool.Resources.Globalization.Messages", typeof(Messages).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deploy to:.
        /// </summary>
        public static string DeployToColon {
            get {
                return ResourceManager.GetString("DeployToColon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Do you want to continue?.
        /// </summary>
        public static string DoYouWantToContinueQuestionmark {
            get {
                return ResourceManager.GetString("DoYouWantToContinueQuestionmark", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ERROR: {0} process ended with Error..
        /// </summary>
        public static string ErrorXProcessEndedWithError {
            get {
                return ResourceManager.GetString("ErrorXProcessEndedWithError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid input..
        /// </summary>
        public static string InvalidInput {
            get {
                return ResourceManager.GetString("InvalidInput", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid report suffix for &apos;{0}&apos; report upload mode. Suffix value=[{1}].
        /// </summary>
        public static string InvalidReportSufixForXReportUploadModeSufixValueY {
            get {
                return ResourceManager.GetString("InvalidReportSufixForXReportUploadModeSufixValueY", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid report report upload mode. Value=[{0}].
        /// </summary>
        public static string InvalidReportUploadModeValueX {
            get {
                return ResourceManager.GetString("InvalidReportUploadModeValueX", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Invalid {0} for {1}..
        /// </summary>
        public static string InvalidXForY {
            get {
                return ResourceManager.GetString("InvalidXForY", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Log Empty.
        /// </summary>
        public static string LogEmpty {
            get {
                return ResourceManager.GetString("LogEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Provide credentials for:.
        /// </summary>
        public static string ProvideCredentialsForColon {
            get {
                return ResourceManager.GetString("ProvideCredentialsForColon", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Report {0} has more than one &quot;{1}&quot; parameter..
        /// </summary>
        public static string ReportXHasMoreThanOneYParameter {
            get {
                return ResourceManager.GetString("ReportXHasMoreThanOneYParameter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to There are no reports to smoke.
        /// </summary>
        public static string ThereAreNoReportsToSmoke {
            get {
                return ResourceManager.GetString("ThereAreNoReportsToSmoke", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User name, password or user domain is empty..
        /// </summary>
        public static string UserNamePasswordOrUserDomainIsEmpty {
            get {
                return ResourceManager.GetString("UserNamePasswordOrUserDomainIsEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User without permissions..
        /// </summary>
        public static string UserWithoutPermissions {
            get {
                return ResourceManager.GetString("UserWithoutPermissions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to WARNING: {0}.
        /// </summary>
        public static string WarningX {
            get {
                return ResourceManager.GetString("WarningX", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to --- {0} Ended at ({1}) ({2}) ---.
        /// </summary>
        public static string XEndedAtYZ {
            get {
                return ResourceManager.GetString("XEndedAtYZ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} had not been updated.
        /// </summary>
        public static string XHadNotBeenUpdated {
            get {
                return ResourceManager.GetString("XHadNotBeenUpdated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} must have at least one {1}..
        /// </summary>
        public static string XMustHaveAtLeastOneY {
            get {
                return ResourceManager.GetString("XMustHaveAtLeastOneY", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} must have {1}..
        /// </summary>
        public static string XMustHaveY {
            get {
                return ResourceManager.GetString("XMustHaveY", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} process error.
        /// </summary>
        public static string XProcessError {
            get {
                return ResourceManager.GetString("XProcessError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to --- {0} Started at ({1}) ---.
        /// </summary>
        public static string XStartedAtY {
            get {
                return ResourceManager.GetString("XStartedAtY", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} Updated.
        /// </summary>
        public static string XUpdated {
            get {
                return ResourceManager.GetString("XUpdated", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You must select an Environment and a Group..
        /// </summary>
        public static string YouMustSelectAnEnvironmentAndAGroup {
            get {
                return ResourceManager.GetString("YouMustSelectAnEnvironmentAndAGroup", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You must select one or more reports..
        /// </summary>
        public static string YouMustSelectOneOrMoreReports {
            get {
                return ResourceManager.GetString("YouMustSelectOneOrMoreReports", resourceCulture);
            }
        }
    }
}
