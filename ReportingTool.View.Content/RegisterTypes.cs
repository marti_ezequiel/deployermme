﻿using ReportingTool.View.Controls;

namespace ReportingTool.View.Content
{
    public static class RegisterTypes
    {
        public static void Register()
        {
            Struct.Dependency.DependencyResolver.Instance.RegisterType<ICredentialController, CredentialController>();
        }
    }
}
