﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Input;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Interfaces.Logic;
using ReportingTool.Resources.Globalization;
using ReportingTool.Resources.Parameters;
using ReportingTool.Struct.View;

namespace ReportingTool.View.Content
{
    public class DeployConfigurationVM : ViewModel
    {
        #region Attributes And Properties

        private readonly IBrowserDialogPromptControl browserDialog;

        private readonly IDeployConfigurationLogic configurationLogic;
        
        public ICommand ButtonBrowseCommand { get; private set; }

        public ICommand EnterPathCommand { get; private set; }

        private String path;
        private readonly String PathPropertyName = "Path";
        public String Path
        {
            get
            {
                return this.path;
            }
            set
            {
                if (value != this.path)
                {
                    this.path = value;

                    this.RaisePropertyChanged(PathPropertyName);
                }
            }
        }

        private IEnumerable<DeployConfigurationModel> configurations;
        private readonly String ConfigurationsPropertyName = "Configurations";
        public IEnumerable<DeployConfigurationModel> Configurations
        {
            get
            {
                return this.configurations;
            }
            set
            {
                if (value != this.configurations)
                {
                    this.configurations = value;

                    this.RaisePropertyChanged(ConfigurationsPropertyName);
                }
            }
        }

        private DeployConfigurationModel selectedConfiguration;
        private readonly String SelectedConfigurationPropertyName = "SelectedConfiguration";
        public DeployConfigurationModel SelectedConfiguration
        {
            get
            {
                return this.selectedConfiguration;
            }
            set
            {
                if (value != this.selectedConfiguration)
                {
                    this.selectedConfiguration = value;

                    this.RaisePropertyChanged(this.SelectedConfigurationPropertyName);
                }
            }
        }

        private Boolean isFolderChecked;
        private readonly String IsFolderCheckedPropertyName = "IsFolderChecked";
        public Boolean IsFolderChecked
        {
            get
            {
                return this.isFolderChecked;
            }
            set
            {
                if (value != this.isFolderChecked)
                {
                    this.isFolderChecked = value;

                    this.RaisePropertyChanged(this.IsFolderCheckedPropertyName);
                }
            }
        }

        private Boolean isXmlFileChecked;
        private readonly String IsXmlFileCheckedPropertyName = "IsXmlFileChecked";
        public Boolean IsXmlFileChecked
        {
            get
            {
                return this.isXmlFileChecked;
            }
            set
            {
                if (value != this.isXmlFileChecked)
                {
                    this.isXmlFileChecked = value;

                    this.RaisePropertyChanged(this.IsXmlFileCheckedPropertyName);
                }
            }
        }

        private ConfigurationSource configurationSource;
        private readonly String ConfigurationSourcePropertyName = "ConfigurationSource";
        public ConfigurationSource ConfigurationSource 
        {
            get
            {
                return this.configurationSource;
            }
            set
            {
                if (value != this.configurationSource)
                {
                    this.configurationSource = value;

                    this.RaisePropertyChanged(this.ConfigurationSourcePropertyName);
                }
            }
        }
        
        #endregion

        #region Events

        public event ConfigurationSourceHandler ConfigurationSourceChange;

        public delegate void ConfigurationSourceHandler(ConfigurationSource configurationSourceValue);

        #endregion

        #region Constructor

        public DeployConfigurationVM(IBrowserDialogPromptControl browserDialog, IDeployConfigurationLogic configurationLogic)
        {
            this.Path = Settings.ConfigXmlFilePath;

            this.configurationLogic = configurationLogic;

            this.browserDialog = browserDialog;

            this.IsXmlFileChecked = true;

            this.LoadFile(false);
        }

        #endregion

        #region Methods

        protected override void AddCommands()
        {
            base.AddCommands();

            this.EnterPathCommand = new Command<String>((s) => this.LoadFile());

            this.ButtonBrowseCommand = new Command<String>((s) => this.SelectPath());
        }

        protected override void OnPropertyChanged(String propertyName)
        {
            switch (propertyName)
            {
                case "IsXmlFileChecked":
                    this.OnSwitchToXml();
                    break;
                case "IsFolderChecked":
                    this.OnSwitchToFolder();
                    break;
                case "ConfigurationSource":
                    this.ConfigurationSourceChange(this.ConfigurationSource);
                    break;
            }
        }

        private void OnSwitchToXml()
        {
            if (!this.IsXmlFileChecked ||
                this.Path.TrimEnd().EndsWith(Labels.DotXml, StringComparison.InvariantCultureIgnoreCase)) return;

            try
            {
                if (File.GetAttributes(this.Path).HasFlag(FileAttributes.Directory))
                {
                    this.Path = String.Format(Masks.XInvertedSlashY, this.Path, Labels.FileNameXmlConfigXml);

                    this.LoadFile(false);
                }
            }
            catch { }
        }

        private void OnSwitchToFolder()
        {
            try
            {
                if (!this.IsFolderChecked ||
                    File.GetAttributes(this.Path).HasFlag(FileAttributes.Directory)) return;

                this.Path = System.IO.Path.GetDirectoryName(this.Path);

                this.LoadFile(false);
            }
            catch { }
        }

        private void SelectPath()
        {
            String path = this.browserDialog.SelectPath(Labels.FileNameXmlConfigXml.Split('.')[0], Labels.FileNameXmlConfigXml.Split('.')[1], String.Empty, this.IsFolderChecked);

            if (!String.IsNullOrEmpty(path))
            {
                try
                {
                    this.Path = path;

                    this.LoadFile();

                    Settings.ConfigXmlFilePath = path;
                }
                catch (Exception ex)
                {
                    this.ShowError(ex);
                }
            }
        }

        private void LoadFile(Boolean promptError = true)
        {
            ConfigurationSource sourceResolver = ConfigurationSource.None;

            try
            {
                this.Configurations = configurationLogic.GetConfiguration(this.Path, ref sourceResolver);

                this.ConfigurationSource = sourceResolver;

                switch (sourceResolver)
                {
                    case ConfigurationSource.XmlFile:
                        this.IsXmlFileChecked = true;
                        break;

                    case ConfigurationSource.Folder:
                        this.IsFolderChecked = true;
                        break;
                }
            }
            catch (Exception ex)
            {
                if (promptError) this.ShowError(ex);
            }
        }

        #endregion
    }
}
