﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Resources.Globalization;
using ReportingTool.Struct.View;

namespace ReportingTool.View.Content
{
    public class DeployLogViewModel : ViewModel
    {
        #region Enumerations

        public enum Color
        {
            Default,
            Black,
            Red,
            Orange,
            Green,
            Blue
        }

        public enum Bold
        {
            Normal,
            SemiBold,
            Bold,
            Bolder
        }

        private readonly Dictionary<Color, Brush> Colors = new Dictionary<Color, Brush>()
        {
            { Color.Default, Brushes.Black },
            { Color.Black, Brushes.Black },
            { Color.Red, Brushes.Red },
            { Color.Orange, Brushes.Goldenrod },
            { Color.Green, Brushes.Green },
            { Color.Blue, Brushes.DarkBlue }
        };

        private readonly Dictionary<Bold, FontWeight> Bolds = new Dictionary<Bold, FontWeight>()
        {
            { Bold.Normal, FontWeights.Normal },
            { Bold.SemiBold, FontWeights.SemiBold },
            { Bold.Bold, FontWeights.Bold },
            { Bold.Bolder, FontWeights.ExtraBold }
        };

        #endregion

        #region Properties

        private readonly String InlineListPropertyName = nameof(InlineList);
        public ObservableCollection<Inline> InlineList { get; set; }

        #endregion

        #region CustomProperties

        public String Text
        {
            get
            {
                return this.InlineList != null && this.InlineList.Any() ? 
                    String.Join(String.Empty, this.InlineList.Select(i => ((Run)i).Text)) : 
                    String.Empty;
            }
        }

        #endregion

        #region Constructor

        public DeployLogViewModel()
        {
            this.InlineList = new ObservableCollection<Inline>();
        }

        #endregion

        #region Methods

        public void Clear()
        {
            this.InlineList.Clear();

            this.RaisePropertyChanged(InlineListPropertyName);
        }

        #region Process

        public void StartProcess(ProcessControl process, DeployConfigurationModel group)
        {
            process.Start();

            this.LogMessage(String.Format(Messages.XStartedAtY, process, process.StartTime.Value), true, Color.Green, Bold.SemiBold);

            this.LogGroupInformation(group);
        }

        private void LogGroupInformation(DeployConfigurationModel group)
        {
            this.LogMessage(Labels.ReportServerColon, true, Color.Black, Bold.Bold); this.LogMessage(String.Concat(Labels.UrlColon, group.ServerServiceUrl), false, Color.Black, Bold.SemiBold);
            this.LogMessage(Labels.ReportServerColon, true, Color.Black, Bold.Bold); this.LogMessage(String.Concat(Labels.ReportFolderColon, group.ServerReportFolder), false, Color.Black, Bold.SemiBold);
            this.LogMessage(Labels.ReportServerColon, true, Color.Black, Bold.Bold); this.LogMessage(String.Concat(Labels.DataSetFolderColon, group.ServerDataSetFolder), false, Color.Black, Bold.SemiBold);
            this.LogMessage(Labels.ReportServerColon, true, Color.Black, Bold.Bold); this.LogMessage(String.Concat(Labels.DataSourceFolderColon, group.ServerDataSourceFolder), false, Color.Black, Bold.SemiBold);

            this.LogMessage(Labels.SourceUpperColon, true, Color.Black, Bold.Bold); this.LogMessage(String.Concat(Labels.SourceColon, group.SourceSrc), false, Color.Black, Bold.SemiBold);
            this.LogMessage(Labels.SourceUpperColon, true, Color.Black, Bold.Bold); this.LogMessage(String.Concat(Labels.FolderColon, group.SourceReportFolder), false, Color.Black, Bold.SemiBold);
        }

        public void EndProcess(ProcessControl process)
        {
            process.Stop();

            this.LogMessage(String.Format(Messages.XEndedAtYZ, process, process.EndTime.Value, process.GetEnlapsedTime()), true, Color.Green, Bold.SemiBold);
        }

        #endregion

        #region SmokeTest

        public void LogSmokeTestSuccess(RtReport report)
        {
            this.LogMessage(String.Concat(Labels.ReportTested, Labels.ColonSpace), true, Color.Black, Bold.Bold);

            this.LogMessage(report.Name, false, Color.Blue, Bold.SemiBold);

            this.LogMessage(String.Concat(Labels.Separator, Labels.Passed), false, Color.Blue, Bold.SemiBold);
        }

        public void LogSmokeTestFail(RtReport report, Exception ex)
        {
            this.LogMessage(String.Concat(
                Labels.ReportTested,
                Labels.ColonSpace,
                report.Name,
                Labels.Separator,
                Labels.Failed), true, Color.Orange, Bold.Bolder);

            this.LogMessage(String.Concat(Labels.Separator, ex.Message), false, Color.Orange, Bold.SemiBold);
        }

        public void LogWarningNoSmokeTests()
        {
            this.LogMessage(Messages.ThereAreNoReportsToSmoke, true, Color.Orange, Bold.Bold);
        }

        #endregion

        #region Errors

        public void LogInvalidCredentials(ProcessControl process)
        {
            this.LogMessage(Messages.UserWithoutPermissions, true, Color.Red, Bold.Bold);

            this.EndProcess(process);
        }

        public void LogError(ProcessControl process, Exception ex = default(Exception))
        {
            this.LogMessage(String.Format(Messages.ErrorXProcessEndedWithError, process), true, Color.Red, Bold.Bold);

            if (ex != default(Exception)) this.LogMessage(ex.Message, true, Color.Red, Bold.SemiBold);

            this.EndProcess(process);
        }

        #endregion

        #region Upload

        public void LogUpload(RtReport report, Boolean success)
        {
            if (success) this.LogUploadSuccess(report);
            else this.LogUploadFail(report);
        }

        public void LogDataUpdated(String source, RtReport report, String ds, String reference, Boolean success)
        {
            if (success) this.LogDataHasBeenUpdated(source, report, ds, reference);
            else this.LogDataHasNotBeenUpdated(source, report, ds, reference);
        }

        private void LogUploadSuccess(RtReport report)
        {
            this.LogMessage(String.Format(Masks.XColonSpace, Labels.ReportUploaded), true, Color.Black, Bold.Bold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.FileName),
                    String.Format(Masks.XApostrophes, report.FileNameWithoutExtention),
                    Labels.Separator),
                false, Color.Blue, Bold.SemiBold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.ReportName),
                    String.Format(Masks.XApostrophes, report.Name)),
                false, Color.Blue, Bold.SemiBold);
        }

        private void LogUploadFail(RtReport report)
        {
            this.LogMessage(String.Format(Masks.XColonSpace, Labels.Error), true, Color.Red, Bold.Bold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.FileName),
                    String.Format(Masks.XApostrophes, report.FileNameWithoutExtention),
                    Labels.Separator),
                false, Color.Red, Bold.SemiBold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.ReportName),
                    String.Format(Masks.XApostrophes, report.Name)),
                false, Color.Red, Bold.SemiBold);
        }

        private void LogDataHasBeenUpdated(String source, RtReport report, String ds, String reference)
        {
            this.LogMessage(String.Format(Messages.XUpdated, source), true, Color.Blue, Bold.SemiBold);

            this.LogMessage(
                String.Concat(
                    Labels.Separator,
                    String.Format(Masks.XColonSpace, Labels.ReportName),
                    String.Format(Masks.XApostrophes, report.Name),
                    Labels.Separator),
                false, Color.Blue, Bold.SemiBold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.DatasourceName),
                    String.Format(Masks.XApostrophes, ds),
                    Labels.Separator),
                false, Color.Blue, Bold.SemiBold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.Reference),
                    String.Format(Masks.XApostrophes, reference)),
                false, Color.Blue, Bold.SemiBold);
        }

        private void LogDataHasNotBeenUpdated(String source, RtReport report, String ds, String reference)
        {
            this.LogMessage(String.Format(Messages.XHadNotBeenUpdated, source), true, Color.Orange, Bold.Bold);

            this.LogMessage(
                String.Concat(
                    Labels.Separator,
                    String.Format(Masks.XColonSpace, Labels.ReportName),
                    String.Format(Masks.XApostrophes, report.Name),
                    Labels.Separator),
                false, Color.Orange, Bold.SemiBold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.DatasourceName),
                    String.Format(Masks.XApostrophes, ds),
                    Labels.Separator),
                false, Color.Orange, Bold.SemiBold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.Reference),
                    String.Format(Masks.XApostrophes, reference)),
                false, Color.Orange, Bold.SemiBold);
        }

        public void LogWarning(RtReport report, String message)
        {
            this.LogMessage(String.Format(Masks.XColonSpace, Labels.Warning), true, Color.Orange, Bold.Bold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.ReportName),
                    String.Format(Masks.XApostrophes, report.Name),
                    Labels.Separator),
                false, Color.Orange, Bold.SemiBold);

            this.LogMessage(
                String.Concat(
                    String.Format(Masks.XColonSpace, Labels.Message),
                    String.Format(Masks.XApostrophes, message)),
                false, Color.Orange, Bold.SemiBold);
        }

        #endregion

        private void LogMessage(String text, Boolean isNewLine = true, Color brush = Color.Default, Bold weight = Bold.Normal)
        {
            var time = this.GetFormatedTime(DateTime.Now);

            if (InlineList.Any() && isNewLine) this.InlineList.Add(new Run(Environment.NewLine));

            if (isNewLine)
                this.InlineList.Add(new Run()
                {
                    Text = time
                });

            this.InlineList.Add(new Run()
            {
                Text = text,
                Foreground = Colors[brush],
                FontWeight = Bolds[weight]
            });

            this.RaisePropertyChanged(this.InlineListPropertyName);
        }

        private String GetFormatedTime(DateTime time)
        {
            return String.Format(Masks.BracketsXTime, time);
        }

        #endregion
    }
}
