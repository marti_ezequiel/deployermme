﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using MahApps.Metro.Controls;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Struct.Dependency;

namespace ReportingTool.View.Content
{
    public partial class DeployConfigurationView : UserControl
    {
        #region Properties

        private Boolean SourceChangedRecently { get; set; }

        #endregion

        #region Custom Properties

        private DeployConfigurationVM ViewModel 
        {
            get
            {
                return (DeployConfigurationVM)this.Expander.DataContext;
            }
        }

        public DeployConfigurationModel SelectedConfiguration
        {
            get { return (DeployConfigurationModel)GetValue(SelectedConfigurationProperty); }
            set { SetValue(SelectedConfigurationProperty, value); }
        }

        public ConfigurationSource ConfigurationSource
        {
            get { return (ConfigurationSource)GetValue(ConfigurationSourceProperty); }
            set { SetValue(ConfigurationSourceProperty, value); }
        }

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty SelectedConfigurationProperty =
            DependencyProperty.Register("SelectedConfiguration", typeof(DeployConfigurationModel), typeof(DeployConfigurationView), new PropertyMetadata(default(DeployConfigurationModel)));

        public static readonly DependencyProperty ConfigurationSourceProperty =
            DependencyProperty.Register("ConfigurationSource", typeof(ConfigurationSource), typeof(DeployConfigurationView), new PropertyMetadata(default(ConfigurationSource)));

        #endregion

        #region Events

        private void OnSelectionChanged(Object sender, SelectionChangedEventArgs e)
        {
            var control = (ComboBox)sender;

            this.SelectedConfiguration = (DeployConfigurationModel)control.SelectedItem;

            if (this.SourceChangedRecently)
            {
                this.SourceChangedRecently = false;

                this.Expander.IsExpanded = false;
            }
        }

        private void OnTargetUpdated(Object sender, System.Windows.Data.DataTransferEventArgs e)
        {
            this.SourceChangedRecently = true;

            var control = (ComboBox)sender;

            var itemsSource = control.Items.Cast<DeployConfigurationModel>();

            if (itemsSource != null && (itemsSource.Count() == 1 || itemsSource.Any(i => i.SelectedByDefault)))
            {
                var selectedItem = itemsSource.FirstOrDefault(i => i.SelectedByDefault) ?? itemsSource.Single();

                this.ViewModel.SelectedConfiguration = selectedItem;

                control.SelectedItem = selectedItem;
            }

            this.OnConfigurationSourceChange(this.ViewModel.ConfigurationSource);
        }

        private void OnConfigurationSourceChange(ConfigurationSource configurationSourceValue)
        {
            this.ConfigurationSource = configurationSourceValue;
        }

        #endregion

        #region Constructor

        public DeployConfigurationView() 
        {
            InitializeComponent();

            this.Expander.DataContext = DependencyResolver.Instance.Resolve<DeployConfigurationVM>();

            ControlsHelper.SetHeaderFontSize(this.Expander, 12);

            this.Expander.IsExpanded = true;

            this.ViewModel.ConfigurationSourceChange += this.OnConfigurationSourceChange;
        }

        #endregion
    }
}
