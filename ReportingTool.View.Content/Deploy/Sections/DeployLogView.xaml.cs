﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace ReportingTool.View.Content
{
    public partial class DeployLogView : UserControl
    {
        #region Properties

        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }

        public DeployLogViewModel Control
        {
            get { return (DeployLogViewModel)GetValue(ControlProperty); }
            set { SetValue(ControlProperty, value); }
        }

        public static readonly DependencyProperty ControlProperty =
            DependencyProperty.Register("Control", typeof(DeployLogViewModel), typeof(DeployLogView), new PropertyMetadata(new DeployLogViewModel(), OnControlChanged));

        private static void OnControlChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBlock = (DeployLogView)d;

            textBlock.Container.DataContext = (DeployLogViewModel)e.NewValue;
        }


        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty TextWrappingProperty =
            DependencyProperty.Register("TextWrapping", typeof(TextWrapping), typeof(DeployLogView), new PropertyMetadata(TextWrapping.NoWrap, OnTextWrappingChange));

        #endregion

        #region Events

        private static void OnTextWrappingChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var textBlock = (DeployLogView)d;

            textBlock.TextContainer.TextWrapping = (TextWrapping)e.NewValue;
        }

        private void OnScrollChanged(Object sender, ScrollChangedEventArgs e)
        {
            if (e.ExtentHeightChange != 0)
            {
                var scrollViewer = sender as ScrollViewer;
                scrollViewer?.ScrollToBottom();
            }
        }

        #endregion

        #region Constructor

        public DeployLogView()
        {
            InitializeComponent();
        }

        #endregion
    }
}
