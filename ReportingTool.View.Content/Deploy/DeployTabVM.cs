﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Interfaces.Logic;
using ReportingTool.Exceptions;
using ReportingTool.Resources.Globalization;
using ReportingTool.Struct.View;
using ReportingTool.View.Controls;

namespace ReportingTool.View.Content
{
    public class DeployTabVM : ViewModel
    {
        #region Attributes

        private readonly ICredentialController credentialController;

        private readonly IDeployLogic deployLogic;

        private readonly IBrowserDialogPromptControl browserDialog;

        #endregion

        #region Properties

        private DeployConfigurationModel selectedConfiguration;
        public String SelectedConfigurationPropertyName = nameof(SelectedConfiguration);
        public DeployConfigurationModel SelectedConfiguration
        {
            get
            {
                return this.selectedConfiguration ?? new DeployConfigurationModel();
            }
            set
            {
                if (value != this.selectedConfiguration)
                {
                    this.selectedConfiguration = value;

                    this.RaisePropertyChanged(this.SelectedConfigurationPropertyName);
                }
            }
        }

        private IEnumerable<RtReport> checkedReports;
        private readonly String CheckedReportsPropertyName = "CheckedReports";
        public IEnumerable<RtReport> CheckedReports
        {
            get
            {
                return this.checkedReports;
            }
            set
            {
                if (value != this.checkedReports)
                {
                    this.checkedReports = value;

                    this.RaisePropertyChanged(this.CheckedReportsPropertyName);
                }
            }
        }

        public DeployLogViewModel LogControl { get; set; }

        public Boolean EnableLogWarnings { get; set; }

        private Boolean smokeTestOnUpload;
        private readonly String SmokeTestOnUploadPropertyName = "SmokeTestOnUpload";
        public Boolean SmokeTestOnUpload
        {
            get
            {
                return this.smokeTestOnUpload;
            }
            set
            {
                if (value != this.smokeTestOnUpload)
                {
                    this.smokeTestOnUpload = value;

                    this.RaisePropertyChanged(this.SmokeTestOnUploadPropertyName);
                }
            }
        }

        private ConfigurationSource configurationSource;
        private readonly String ConfigurationSourcePropertyName = "ConfigurationSource";
        public ConfigurationSource ConfigurationSource
        {
            get
            {
                return this.configurationSource;
            }
            set
            {
                if (value != this.configurationSource)
                {
                    this.configurationSource = value;

                    this.RaisePropertyChanged(this.ConfigurationSourcePropertyName);
                }
            }
        }

        private Boolean isEnabledSmokeTest;
        private readonly String IsEnableSmokeTestPropertyName = "IsEnableSmokeTest";
        public Boolean IsEnableSmokeTest
        {
            get
            {
                return this.isEnabledSmokeTest;
            }
            set
            {
                if (value != this.isEnabledSmokeTest)
                {
                    this.isEnabledSmokeTest = value;

                    this.RaisePropertyChanged(this.IsEnableSmokeTestPropertyName);
                }
            }
        }

        #endregion

        #region Commands

        public ICommand ButtonClearLogCommand
        {
            get;
            private set;
        }

        public ICommand ButtonSaveLogCommand
        {
            get;
            private set;
        }

        public ICommand ButtonUploadCommand
        {
            get;
            private set;
        }

        public ICommand ButtonSmokeTestCommand
        {
            get;
            private set;
        }

        #endregion

        #region Events

        public delegate void SelectedConfigurationChange(DeployConfigurationModel selectedConfiguration);

        public event SelectedConfigurationChange OnSelectedConfigurationChange;

        #endregion

        #region Constructor

        public DeployTabVM(IBrowserDialogPromptControl browserDialog, IDeployLogic deployLogic, ICredentialController credentialController) 
        {
            this.LogControl = new DeployLogViewModel();

            this.browserDialog = browserDialog;

            this.deployLogic = deployLogic;

            this.credentialController = credentialController;
        }

        #endregion

        #region Methods

        protected override void AddCommands()
        {
            base.AddCommands();

            this.ButtonClearLogCommand = new Command<String>((s) => this.ClearLog());

            this.ButtonSaveLogCommand = new Command<String>((s) => this.SaveLog());

            this.ButtonUploadCommand = new Command<String>((s) => this.UploadAsync());

            this.ButtonSmokeTestCommand = new Command<String>((s) => this.SmokeTestAsync());
        }

        protected override void OnPropertyChanged(String propertyName)
        {
            switch (propertyName)
            {
                case "ConfigurationSource":
                    this.IsEnableSmokeTest = this.ConfigurationSource == ConfigurationSource.XmlFile;
                    break;

                case "IsEnableSmokeTest":
                    if (!this.IsEnableSmokeTest) this.SmokeTestOnUpload = false;
                    break;

                case "SelectedConfiguration":
                    if (this.OnSelectedConfigurationChange != null)
                        this.OnSelectedConfigurationChange(this.SelectedConfiguration);
                    break;
            }
        }

        private async void UploadAsync()
        {
            if (!this.ValidateEntrance()) return;

            var process = new ProcessControl(Labels.Deploy);

            try
            {
                if (!this.SelectedConfiguration.IsProdServerOrStageServer ||
                    this.Confirm(
                        String.Concat(Messages.DeployToColon, this.SelectedConfiguration.ServerServiceUrl, System.Environment.NewLine, Messages.DoYouWantToContinueQuestionmark), 
                        Labels.HighEnvironmentDeploy))
                {
                    var credential = this.credentialController.GetCredentials(this.SelectedConfiguration.ServerServiceUrl);

                    if (credential != null)
                    {
                        var log = new Progress<Action<DeployLogViewModel>>(a => a.Invoke(this.LogControl));

                        await System.Threading.Tasks.Task.Run(() => this.UploadReport(this.SelectedConfiguration, this.CheckedReports, process, credential, this.EnableLogWarnings, log));
                    }
                }
            }
            catch (CredentialException)
            {
                this.LogControl.LogInvalidCredentials(process);

                this.credentialController.ClearCredentials();

                return;
            }
            catch (Exception ex)
            {
                this.LogControl.LogError(process, ex);
            }
        }

        private void UploadReport(DeployConfigurationModel group, IEnumerable<RtReport> reports, ProcessControl process, CredentialEntity credential, Boolean enableParamWarning, IProgress<Action<DeployLogViewModel>> log)
        {
            var logUpload = new Progress<UploadStatus>(s => log.Report(l => l.LogUpload(s.Report, s.IsSuccess)));

            var logWarning = enableParamWarning ?
                new Progress<WarningStatus>(s => log.Report(l => l.LogWarning(s.Report, s.Message))) :
                new Progress<WarningStatus>();

            var logDataSource = new Progress<DataSourceStatus>(s => log.Report(l => l.LogDataUpdated(s.Source, s.Report, s.Ds, s.Reference, s.IsSuccess)));

            log.Report(l => l.StartProcess(process, group));

            var reportsForSmokeTest = this.deployLogic.UploadReport(group , reports, credential, logUpload, logWarning, logDataSource);

            log.Report(l => l.EndProcess(process));

            if (this.SmokeTestOnUpload && reportsForSmokeTest.Any())
            {
                this.SmokeTest(new ProcessControl(Labels.SmokeTest), group, reportsForSmokeTest, credential, log);
            }
        }
        
        private async void SmokeTestAsync()
        {
            if (!this.ValidateEntrance()) return;

            var process = new ProcessControl(Labels.SmokeTest);

            try
            {
                if (!this.CheckedReports.Any())
                {
                    this.LogControl.LogWarningNoSmokeTests();

                    return;
                }

                var credential = this.credentialController.GetCredentials(this.SelectedConfiguration.ServerServiceUrl);

                if (credential != null)
                {
                    var log = new Progress<Action<DeployLogViewModel>>(a => a.Invoke(this.LogControl));

                    await System.Threading.Tasks.Task.Run(() => this.SmokeTest(process, this.SelectedConfiguration, this.CheckedReports, credential, log));
                }
            }
            catch(Exception ex)
            {
                this.LogControl.LogError(process, ex);
            }
        }

        private void SmokeTest(ProcessControl process, DeployConfigurationModel group, IEnumerable<RtReport> reports, CredentialEntity credential, IProgress<Action<DeployLogViewModel>> log)
        {
            if (!this.IsEnableSmokeTest) return;

            log.Report(a => a.StartProcess(process, group));
            
            foreach (RtReport report in reports)
            {
                try
                {
                    if (this.deployLogic.RenderReport(report, group, credential))
                    {
                        log.Report(a => a.LogSmokeTestSuccess(report));
                    }
                }
                catch (CredentialException)
                {
                    log.Report(l => l.LogInvalidCredentials(process));

                    this.credentialController.ClearCredentials();

                    return;
                }
                catch (Exception ex)
                {
                    log.Report(a => a.LogSmokeTestFail(report, ex));
                }
            }

            log.Report(a => a.EndProcess(process));
        }

        private Boolean ValidateEntrance()
        {
            if (this.SelectedConfiguration == default(DeployConfigurationModel))
            {
                this.ShowNotification(Messages.YouMustSelectAnEnvironmentAndAGroup);

                return false;
            }

            if (this.CheckedReports == default(IEnumerable<RtReport>) || !this.CheckedReports.Any())
            {
                this.ShowNotification(Messages.YouMustSelectOneOrMoreReports);

                return false;
            }

            return true;
        }

        private void SaveLog()
        {
            if (String.IsNullOrWhiteSpace(this.LogControl.Text))
            {
                MessageBox.Show(Messages.LogEmpty, Labels.Attention, MessageBoxButton.OK, MessageBoxImage.Information);

                return;
            }

            browserDialog.SaveFile(
                String.Format(Masks.ReportDeployLogXYDateTime, this.SelectedConfiguration.Name, DateTime.Now),
                this.LogControl.Text
            );
        }

        private void ClearLog()
        {
            this.LogControl.Clear();
        }

        #endregion
    }
}
