﻿using ReportingTool.View.Controls;

namespace ReportingTool.View.Content
{
    public partial class DeployTabView : TabItem<DeployTabVM>
    {
        public DeployTabView(DeployTabVM deployTabVM)
            : base(deployTabVM)
        {
            InitializeComponent();
        }

        public void AsociarSmokeTestTab(SmokeTestTabView smokeTestTab)
        {
            this.ViewModel.OnSelectedConfigurationChange += smokeTestTab.ViewModel.SetConfiguration;
        }
    }
}
