﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Windows.Input;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Entities.SmokeTest;
using ReportingTool.Business.Interfaces.Logic;
using ReportingTool.Struct.View;
using ReportingTool.View.Controls;

namespace ReportingTool.View.Content
{
    public class SmokeTestTabVM : ViewModel
    {
        #region Attributes

        private readonly ISmokeTestLogic smokeTestLogic;

        private readonly ICredentialController credentialController;

        #endregion

        #region Properties

        private DeployConfigurationModel selectedConfiguration;
        public String SelectedConfigurationPropertyName = nameof(Configuration);
        public DeployConfigurationModel Configuration
        {
            get
            {
                return this.selectedConfiguration ?? new DeployConfigurationModel();
            }
            set
            {
                if (value != this.selectedConfiguration)
                {
                    this.selectedConfiguration = value;

                    this.RaisePropertyChanged(this.SelectedConfigurationPropertyName);
                }
            }
        }

        private ObservableCollection<RtReport> checkedReports;
        private readonly String CheckedReportsPropertyName = "CheckedReports";
        public ObservableCollection<RtReport> CheckedReports
        {
            get
            {
                return this.checkedReports;
            }
            set
            {
                if (value != this.checkedReports)
                {
                    this.checkedReports = value;

                    this.RaisePropertyChanged(this.CheckedReportsPropertyName);
                }
            }
        }

        public IEnumerable<RdlParameter> Parameters { private get; set; }

        public ICommand ButtonSmokeTestCommand
        {
            get;
            private set;
        }

        #endregion

        #region Constructor

        public SmokeTestTabVM(ISmokeTestLogic smokeTestLogic, ICredentialController credentialController)
        {
            this.smokeTestLogic = smokeTestLogic;

            this.credentialController = credentialController;
        }

        #endregion

        #region Methods

        public void SetConfiguration(DeployConfigurationModel configuration)
        {
            this.Configuration = configuration;
        }

        protected override void AddCommands()
        {
            base.AddCommands();

            this.ButtonSmokeTestCommand = new Command<String>((s) => this.SmokeTest());
        }

        private void SmokeTest()
        {
            var credential = this.credentialController.GetCredentials(this.Configuration.ServerServiceUrl);

            if (credential != null)
            {
                this.smokeTestLogic.SmokeTest(this.CheckedReports, this.Parameters, this.Configuration, credential);
            }
        }

        #endregion
    }
}
