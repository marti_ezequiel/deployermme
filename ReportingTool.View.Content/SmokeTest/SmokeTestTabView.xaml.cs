﻿using ReportingTool.View.Controls;

namespace ReportingTool.View.Content
{
    public partial class SmokeTestTabView : TabItem<SmokeTestTabVM>
    {
        public SmokeTestTabView(SmokeTestTabVM smokeTestTabVM)
            : base(smokeTestTabVM)
        {
            InitializeComponent();
        }
    }
}
