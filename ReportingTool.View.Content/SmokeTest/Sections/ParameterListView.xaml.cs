﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Entities.SmokeTest;
using ReportingTool.Struct.Dependency;

namespace ReportingTool.View.Content
{
    public partial class ParameterListView : UserControl
    {
        #region Properties
        
        #endregion

        #region Custom Properties

        private ParameterListViewModel ViewModel 
        {
            get
            {
                return (ParameterListViewModel)this.Container.DataContext;
            }
        }

        public IEnumerable<RtReport> ItemsSource 
        {
            get { return (IEnumerable<RtReport>)GetValue(ItemsSourceProperty); }
            set { SetValue(ItemsSourceProperty, value); }
        }

        public IEnumerable<RdlParameter> Parameters 
        {
            get { return (IEnumerable<RdlParameter>)GetValue(ParametersProperty); }
            set { SetValue(ParametersProperty, value); }
        }

        public Boolean UnifyParameters 
        {
            get { return (Boolean)GetValue(UnifyParametersProperty); }
            set { SetValue(UnifyParametersProperty, value); }
        }

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty ItemsSourceProperty =
            DependencyProperty.Register("ItemsSource", typeof(IEnumerable<RtReport>), typeof(ParameterListView), new PropertyMetadata(new List<RtReport>(), OnItemsSourceChange));
        
        public static readonly DependencyProperty ParametersProperty =
            DependencyProperty.Register("Parameters", typeof(IEnumerable<RdlParameter>), typeof(ParameterListView), new PropertyMetadata(new List<RdlParameter>()));


        public static readonly DependencyProperty UnifyParametersProperty =
            DependencyProperty.Register("UnifyParameters", typeof(Boolean), typeof(ParameterListView), new PropertyMetadata(false, OnChangeUnifyParameters));
        
        #endregion

        #region Events

        private static void OnItemsSourceChange(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ParameterListView view = (ParameterListView)d;

            var newList = (IEnumerable<RtReport>)e.NewValue;

            if (view.ViewModel.ItemsSource != null && newList != null)
            {
                foreach (var item in view.ViewModel.ItemsSource.Where(i => !newList.Contains(i)).ToList()) view.ViewModel.ItemsSource.Remove(item);

                foreach (var item in newList.Where(i => !view.ViewModel.ItemsSource.Contains(i)).ToList()) view.ViewModel.ItemsSource.Add(item);
            }
        }

        private static void OnChangeUnifyParameters(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ParameterListView view = (ParameterListView)d;

            view.ViewModel.UnifyParameters = (Boolean)e.NewValue;
        }

        private void OnParametersChange(Object sender, DataGridCellEditEndingEventArgs e)
        {
            DataGrid dataGrid = sender as DataGrid;

            if (dataGrid != null)
            {
                this.OnParametersChange(dataGrid.Items.Cast<RdlParameter>());
            }
        }

        private void OnParametersChange(IEnumerable<RdlParameter> parameters)
        {
            this.Parameters = parameters;
        }

        #endregion

        #region Constructor

        public ParameterListView()
        {
            InitializeComponent();

            this.Container.DataContext = DependencyResolver.Instance.Resolve<ParameterListViewModel>();

            this.ViewModel.OnParametersChange += this.OnParametersChange;
        }

        #endregion

        #region Methods

        #endregion
    }
}
