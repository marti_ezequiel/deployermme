﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Entities.SmokeTest;
using ReportingTool.Business.Interfaces.Logic;
using ReportingTool.Struct.View;

namespace ReportingTool.View.Content
{
    public class ParameterListViewModel : ViewModel
    {
        #region Attributes

        private readonly ISmokeTestLogic smokeTestLogic;

        #endregion

        #region Properties

        private ObservableCollection<RtReport> itemsSource;
        private readonly String ItemsSourcePropertyName = "ItemsSource";
        public ObservableCollection<RtReport> ItemsSource 
        {
            get
            {
                return this.itemsSource;
            }
            set
            {
                if (value != this.itemsSource)
                {
                    this.itemsSource = value;

                    this.LoadParameters(this.ItemsSource);
                    this.RaisePropertyChanged(this.ItemsSourcePropertyName);
                }
            }
        }
        
        private ObservableCollection<RdlParameter> parameters;
        private readonly String ParametersPropertyName = "Parameters";
        public ObservableCollection<RdlParameter> Parameters
        {
            get
            {
                return this.parameters;
            }
            set
            {
                if (value != this.parameters)
                {
                    this.parameters = value;

                    this.RaisePropertyChanged(this.ParametersPropertyName);
                }
            }
        }

        private Boolean unifyParameters;
        private readonly String UnifyParametersPropertyName = "UnifyParameters";
        public Boolean UnifyParameters
        {
            get
            {
                return this.unifyParameters;
            }
            set
            {
                if (value != this.unifyParameters)
                {
                    this.unifyParameters = value;

                    this.RaisePropertyChanged(this.UnifyParametersPropertyName);
                }
            }
        }

        private Boolean IgnoreUnify { get; set; }

        #endregion

        #region Events

        public delegate void ParametersChange(IEnumerable<RdlParameter> parameters);

        public event ParametersChange OnParametersChange;

        private void OnItemsSourceChanged(Object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    this.LoadParameters(e.NewItems.Cast<RtReport>());
                    break;

                case NotifyCollectionChangedAction.Remove:
                    this.UnloadParameters(e.OldItems.Cast<RtReport>());
                    break;

                case NotifyCollectionChangedAction.Replace:
                    break;

                case NotifyCollectionChangedAction.Move:
                    break;

                case NotifyCollectionChangedAction.Reset:
                    this.Parameters.Clear();
                    break;
            }
        }

        private void OnParameterValueChange(RdlParameter sender, Object newValue)
        {
            if (this.UnifyParameters && !this.IgnoreUnify)
                this.UnifyAllParameters(sender, newValue);
        }

        #endregion

        #region Constructor

        public ParameterListViewModel(ISmokeTestLogic smokeTestLogic) 
        {
            this.smokeTestLogic = smokeTestLogic;

            this.Parameters = new ObservableCollection<RdlParameter>();

            this.ItemsSource = new ObservableCollection<RtReport>();

            this.ItemsSource.CollectionChanged += OnItemsSourceChanged;
        }

        #endregion

        #region Methods

        protected override void OnPropertyChanged(String propertyName)
        {
            switch (propertyName)
            {
                case "UnifyParameters":
                    if (this.UnifyParameters) this.UnifyAllParameters();
                    break;
            }
        }

        private void LoadParameters(IEnumerable<RtReport> reports)
        {
            foreach (var parameter in this.smokeTestLogic.GetParameters(reports))
            {
                if (this.UnifyParameters) parameter.Value = this.GetUnifiedValue(parameter);

                parameter.OnValueChange += this.OnParameterValueChange;

                this.Parameters.Add(parameter);
            };

            if (this.OnParametersChange != null)
                this.OnParametersChange(this.Parameters);
        }

        private void UnloadParameters(IEnumerable<RtReport> reports)
        {
            foreach (var report in reports)
            {
                var parameters = this.Parameters.Where(p => p.ReportFileName.Equals(report.FileName, StringComparison.CurrentCulture)).ToList();

                foreach (var parameter in parameters)
                {
                    parameter.OnValueChange -= OnParameterValueChange;

                    this.Parameters.Remove(parameter);
                }
            };
            
            if (this.OnParametersChange != null)
                this.OnParametersChange(this.Parameters);
        }

        private Object GetUnifiedValue(RdlParameter parameter)
        {
            var firstParameter = this.Parameters.FirstOrDefault(p => 
                p.Name.Equals(parameter.Name, StringComparison.CurrentCultureIgnoreCase) &&
                p.DataType.Equals(parameter.DataType) &&
                p.Value != null
                );

            return firstParameter == default(RdlParameter) ? parameter.Value : firstParameter.Value;
        }

        private void UnifyAllParameters()
        {
            this.IgnoreUnify = true;

            foreach (var parameter in this.Parameters) parameter.Value = this.GetUnifiedValue(parameter);

            this.IgnoreUnify = false;
        }

        private void UnifyAllParameters(RdlParameter reference, Object value)
        {
            this.IgnoreUnify = true;

            foreach (var parameter in this.Parameters.Where(p =>
                p != reference &&
                p.Name.Equals(reference.Name, StringComparison.CurrentCultureIgnoreCase) &&
                p.DataType.Equals(reference.DataType)).ToList())
            {
                parameter.Value = value;
            }
            
            this.IgnoreUnify = false;
        }

        #endregion
    }
}
