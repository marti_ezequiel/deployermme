﻿using System.Windows;
using ReportingTool.Struct.Dependency;
using ReportingTool.Views;

namespace ReportingTool
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            View.Content.RegisterTypes.Register();

            var mainWindow = DependencyResolver.Instance.Resolve<MainWindowView>();

            mainWindow.Show();
        }
    }
}
