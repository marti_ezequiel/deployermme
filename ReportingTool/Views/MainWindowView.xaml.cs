﻿using MahApps.Metro.Controls;
using ReportingTool.Struct.Dependency;

namespace ReportingTool.Views
{
    public partial class MainWindowView : MetroWindow
    {
        public MainWindowView()
        {
            InitializeComponent();

            this.DataContext = DependencyResolver.Instance.Resolve<MainWindowVM>();

            this.tabControl.SelectedIndex = 0;
        }
    }
}
