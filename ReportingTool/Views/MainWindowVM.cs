﻿using System.Collections.ObjectModel;
using ReportingTool.Struct.View;
using ReportingTool.View.Content;
using ReportingTool.View.Controls;

namespace ReportingTool.Views
{
    public class MainWindowVM : ViewModel
    {
        public ObservableCollection<TabItem> TabItems { get; set; }

        public MainWindowVM(DeployTabView deployTab, SmokeTestTabView smokeTestTab)
        {
            deployTab.AsociarSmokeTestTab(smokeTestTab);

            this.TabItems = new ObservableCollection<TabItem>()
            {
                deployTab,
                smokeTestTab
            };
        }
    }
}
