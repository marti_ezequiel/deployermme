﻿using Unity;

namespace ReportingTool.Struct.Dependency
{
    public class DependencyResolver
    {
        private static DependencyResolver dependency;

        public static DependencyResolver Instance
        {
            get
            {
                if (dependency == null) dependency = new DependencyResolver();

                return dependency;
            }
        }

        private IUnityContainer Unity
        {
            get;
            set;
        }

        private DependencyResolver()
        {
            Unity = new UnityContainer();

            Unity.RegisterSingleton<View.IBrowserDialogPromptControl, View.BrowserDialogPromptControl>();

            Unity.RegisterType<Business.Logic.IDeployXmlConfigurationReader, Business.Logic.DeployXmlConfigurationReader>();
            Unity.RegisterType<Business.Logic.IDeployConfigurationReader, Business.Logic.DeployConfigurationReader>();
            Unity.RegisterType<Business.Logic.IDeployFolderConfigurationReader, Business.Logic.DeployFolderConfigurationReader>();

            Unity.RegisterType<Business.Interfaces.Logic.IXmlFileSerializer, Business.Logic.XmlFileSerializer>();
            Unity.RegisterType<Business.Interfaces.Logic.IDeployLogic, Business.Logic.DeployLogic>();
            Unity.RegisterType<Business.Interfaces.Logic.IConfigurationLogic, Business.Logic.ConfigurationLogic>();
            Unity.RegisterType<Business.Interfaces.Logic.IDeployConfigurationLogic, Business.Logic.DeployConfigurationLogic>();
            Unity.RegisterType<Business.Interfaces.Logic.ISmokeTestLogic, Business.Logic.SmokeTestLogic>();
            Unity.RegisterType<Business.Interfaces.Logic.IReportsParameterReader, Business.Logic.ReportsParameterReader>();

            Unity.RegisterType<Access.Adapter.Interfaces.IDeployAdapter, Access.Adapter.DeployAdapter>();
            Unity.RegisterType<Access.Adapter.Interfaces.IConfigurationAdapter, Access.Adapter.ConfigurationAdapter>();
        }

        public void RegisterType<TFrom, TTo>() where TTo : TFrom
        {
            Unity.RegisterType<TFrom, TTo>();
        }

        public T Resolve<T>()
        {
            return this.Unity.Resolve<T>();
        }
    }
}
