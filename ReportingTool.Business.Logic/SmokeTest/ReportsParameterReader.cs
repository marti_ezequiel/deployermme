﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using ReportingTool.Business.Entities.SmokeTest;
using ReportingTool.Business.Interfaces.Logic;

namespace ReportingTool.Business.Logic
{
    public class ReportsParameterReader : IReportsParameterReader
    {
        public IEnumerable<RdlParameter> GetParameters(String reportPath)
        {
            String reportFileName = new FileInfo(reportPath).Name;
            XmlDocument rdlReport = this.GetXmlFile(reportPath);

            XmlNamespaceManager ns = new XmlNamespaceManager(rdlReport.NameTable);

            String designer = rdlReport.DocumentElement.GetAttribute("xmlns:rd");
            String definition = rdlReport.DocumentElement.GetAttribute("xmlns");

            ns.AddNamespace("rd", designer);
            ns.AddNamespace("def", definition);

            var rdlParameters = rdlReport.SelectNodes("/def:Report/def:ReportParameters/def:ReportParameter", ns).Cast<XmlElement>();
            
            foreach (var rdlParameter in rdlParameters)
            {
                String name = rdlParameter.GetAttribute("Name");
                String dataType = rdlParameter.SelectSingleNode("./def:DataType", ns)?.InnerText;
                String defaultValue = rdlParameter.SelectSingleNode("./def:DefaultValue/def:Values/def:Value", ns)?.InnerText;

                yield return new RdlParameter(name, reportFileName, dataType, defaultValue);
            }
        }

        private XmlDocument GetXmlFile(String path)
        {
            Byte[] buffer;

            XmlDocument xmlDocument;

            using (FileStream fileStream = File.OpenRead(path))
            {
                buffer = new Byte[fileStream.Length];

                fileStream.Read(buffer, 0, buffer.Length);

                fileStream.Close();
            }

            using (MemoryStream memoryStream = new MemoryStream(buffer))
            {
                xmlDocument = new XmlDocument();

                xmlDocument.Load(memoryStream);

                memoryStream.Close();
            }

            return xmlDocument;
        }
    }
}
