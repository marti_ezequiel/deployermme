﻿using System.Collections.Generic;
using System.Linq;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Entities.SmokeTest;
using ReportingTool.Business.Interfaces.Logic;

namespace ReportingTool.Business.Logic
{
    public class SmokeTestLogic : ISmokeTestLogic
    {
        private readonly IReportsParameterReader parameterReader;

        private readonly IDeployLogic deployLogic;

        public SmokeTestLogic(IReportsParameterReader parameterReader, IDeployLogic deployLogic)
        {
            this.parameterReader = parameterReader;

            this.deployLogic = deployLogic;
        }

        public IEnumerable<RdlParameter> GetParameters(IEnumerable<RtReport> reports)
        {
            foreach(RtReport report in reports)
            {
                var parameters = parameterReader.GetParameters(report.FullLocalPath);

                foreach(var parameter in parameters)
                {
                    if (report.Params.ContainsKey(parameter.Name))
                    {
                        parameter.Value = report.Params[parameter.Name];
                    }

                    yield return parameter;
                }
            }
        }

        public void SmokeTest(IEnumerable<RtReport> reports, IEnumerable<RdlParameter> parameters, DeployConfigurationModel model, CredentialEntity credentials)
        {
            foreach(var report in reports)
            {
                foreach(RdlParameter parameter in parameters.Where(p => p.ReportFileName == report.FileName))
                {
                    report.Params[parameter.Name] = parameter.Value.ToString();
                }

                this.deployLogic.RenderReport(report, model, credentials);
            }
        }
    }
}
