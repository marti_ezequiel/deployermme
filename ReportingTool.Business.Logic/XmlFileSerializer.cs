﻿using System;
using System.IO;
using System.Xml.Serialization;
using ReportingTool.Business.Interfaces.Logic;

namespace ReportingTool.Business.Logic
{
    public class XmlFileSerializer : IXmlFileSerializer
    {
        public T Read<T>(String path) where T : class
        {
            var xmlFile = File.ReadAllText(path);

            String serializedFile = Serialize(xmlFile);

            return Deserialize<T>(xmlFile);
        }

        private T Deserialize<T>(String input) where T : class
        {
            XmlSerializer ser = new XmlSerializer(typeof(T));

            using (StringReader sr = new StringReader(input))
            {
                return (T)ser.Deserialize(sr);
            }
        }

        private String Serialize<T>(T ObjectToSerialize)
        {
            XmlSerializer xmlSerializer = new XmlSerializer(ObjectToSerialize.GetType());

            using (StringWriter textWriter = new StringWriter())
            {
                xmlSerializer.Serialize(textWriter, ObjectToSerialize);
                return textWriter.ToString();
            }
        }
    }
}
