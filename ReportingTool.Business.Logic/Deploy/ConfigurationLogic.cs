﻿using ReportingTool.Access.Adapter.Interfaces;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Interfaces.Logic;

namespace ReportingTool.Business.Logic
{
    public class ConfigurationLogic : IConfigurationLogic
    {
        private IConfigurationAdapter configurationAdapter;

        public ConfigurationLogic(IConfigurationAdapter adapter)
        {
            this.configurationAdapter = adapter;
        }

        public CredentialEntity GetCredentials()
        {
            return this.configurationAdapter.GetCredentials();
        }

        public void SetCredentials(CredentialEntity credentials)
        {
            this.configurationAdapter.SetCredentials(credentials);
        }

        public void ClearCredentials()
        {
            this.configurationAdapter.ClearCredentials();
        }
    }
}
