﻿using System;
using System.Collections.Generic;
using ReportingTool.Access.Adapter.Interfaces;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Interfaces.Logic;
using ReportingTool.Exceptions;

namespace ReportingTool.Business.Logic
{
    public class DeployLogic : IDeployLogic
    {
        private IDeployAdapter deployAdapter;

        IConfigurationLogic configurationLogic;

        public DeployLogic(IDeployAdapter adapter, IConfigurationLogic configurationLogic)
        {
            this.deployAdapter = adapter;

            this.configurationLogic = configurationLogic;
        }

        public Boolean RenderReport(RtReport report, DeployConfigurationModel group, CredentialEntity credential)
        {
            try
            {
                return this.deployAdapter.RenderReport(report, group, credential);
            }
            catch (CredentialException)
            {
                this.configurationLogic.ClearCredentials();

                throw;
            }
            catch
            {
                throw;
            }
        }

        public void CheckCredentials(CredentialEntity credential)
        {
            this.CheckCredentials(credential);
        }

        public IEnumerable<RtReport> UploadReport(DeployConfigurationModel group, IEnumerable<RtReport> reports, CredentialEntity credential, IProgress<UploadStatus> logUpload, IProgress<WarningStatus> logWarning, IProgress<DataSourceStatus> logDataSource)
        {
            try
            {
                return this.deployAdapter.UploadReport(group, reports, credential, logUpload, logWarning, logDataSource);
            }
            catch (CredentialException)
            {
                this.configurationLogic.ClearCredentials();

                throw;
            }
            catch
            {
                throw;
            }
        }
    }
}
