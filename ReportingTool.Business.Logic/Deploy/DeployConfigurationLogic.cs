﻿using System;
using System.Collections.Generic;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Interfaces.Logic;

namespace ReportingTool.Business.Logic
{
    public class DeployConfigurationLogic : IDeployConfigurationLogic
    {
        private readonly IDeployConfigurationReader reader;

        public DeployConfigurationLogic(IDeployConfigurationReader reader)
        {
            this.reader = reader;
        }

        public IEnumerable<DeployConfigurationModel> GetConfiguration(String path, ref ConfigurationSource resolver)
        {
            return this.reader.GetConfiguration(path, ref resolver);
        }
    }
}
