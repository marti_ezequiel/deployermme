﻿using System;
using System.Collections.Generic;
using ReportingTool.Business.Entities.Deploy;

namespace ReportingTool.Business.Logic
{
    public interface IDeployConfigurationReaderHandler
    {
        IEnumerable<DeployConfigurationModel> GetConfiguration(String path, ref ConfigurationSource resolver);

        void AddHandler(IDeployConfigurationReaderHandler successor);
    }
}
