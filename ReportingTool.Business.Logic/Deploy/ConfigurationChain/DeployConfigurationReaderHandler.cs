﻿using System;
using System.Collections.Generic;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Logic
{
    public abstract class DeployConfigurationReaderHandler : IDeployConfigurationReaderHandler
    {
        private IDeployConfigurationReaderHandler successor;

        public abstract ConfigurationSource Configuration { get; }

        public void AddHandler(IDeployConfigurationReaderHandler successor)
        {
            if (this.successor == null)
            {
                this.successor = successor;

                return;
            }

            this.successor.AddHandler(successor);
        }

        public IEnumerable<DeployConfigurationModel> GetConfiguration(String path, ref ConfigurationSource resolver)
        {
            if (!this.IsValid(path))
            {
                if (this.successor != null) return this.successor.GetConfiguration(path, ref resolver);

                throw new InvalidOperationException(Messages.InvalidInput);
            }

            resolver = this.Configuration;

            return this.Resolve(path);
        }

        protected abstract Boolean IsValid(String path);

        protected abstract IEnumerable<DeployConfigurationModel> Resolve(String path);
        
        protected readonly Dictionary<String, UploadMode> GetUploadMode = new Dictionary<String, UploadMode>()
        {
            { String.Empty, UploadMode.DeleteExisting },
            { Labels.OverWrite, UploadMode.OverWrite },
            { Labels.DeleteExisting, UploadMode.DeleteExisting },
            { Labels.RenameExisting, UploadMode.RenameExisting },
            { Labels.RenameNew, UploadMode.RenameNew }
        };
    }
}
