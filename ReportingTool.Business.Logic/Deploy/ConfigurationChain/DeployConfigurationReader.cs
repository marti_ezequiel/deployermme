﻿using System;
using System.Collections.Generic;
using ReportingTool.Business.Entities.Deploy;

namespace ReportingTool.Business.Logic
{
    public class DeployConfigurationReader : IDeployConfigurationReader
    {
        private IDeployConfigurationReaderHandler Handler { get; set; }

        public DeployConfigurationReader(IDeployXmlConfigurationReader xmlHandler, IDeployFolderConfigurationReader folderHandler)
        {
            this.Handler = xmlHandler;

            xmlHandler.AddHandler(folderHandler);
        }

        public IEnumerable<DeployConfigurationModel> GetConfiguration(String path, ref ConfigurationSource resolver)
        {
            return this.Handler.GetConfiguration(path, ref resolver);
        }
    }
}
