﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Resources.Globalization;
using ReportingTool.Resources.Parameters;

namespace ReportingTool.Business.Logic
{
    public class DeployFolderConfigurationReader : DeployConfigurationReaderHandler, IDeployFolderConfigurationReader
    {
        #region Properties

        private readonly Dictionary<String, String> subFolderDictionary;

        public override ConfigurationSource Configuration
        {
            get
            {
                return ConfigurationSource.Folder;
            }
        }

        #endregion

        #region Constructor

        public DeployFolderConfigurationReader()
        {
            this.subFolderDictionary = Settings.SubFolderReports;
        }

        #endregion

        #region Methods

        protected override Boolean IsValid(String path)
        {
            return File.GetAttributes(path).HasFlag(FileAttributes.Directory);
        }

        protected override IEnumerable<DeployConfigurationModel> Resolve(String path)
        {
            var defaultEnvironments = JsonConvert.DeserializeObject<Entities.Configuration.Environment[]>(Settings.DefaultEnvironments);

            IEnumerable<String> reportPaths = Directory.GetFiles(path, Labels.RdlFilePattern, SearchOption.TopDirectoryOnly);

            var reports = this.GetReports(reportPaths);

            return defaultEnvironments.Select(r => this.ParseToConfiguration(r, path, reports));
        }

        private IEnumerable<RtReport> GetReports(IEnumerable<String> reportsFileList)
        {
            var reports = reportsFileList.Select(r => new RtReport
            {
                FullLocalPath = r
            }).ToList();

            foreach(var report in reports)
            {
                if (subFolderDictionary.ContainsKey(report.FileName))
                    report.ServerSubFolder = subFolderDictionary[report.FileName];

                if (subFolderDictionary.ContainsKey(report.FileNameWithoutExtention))
                    report.ServerSubFolder = subFolderDictionary[report.FileNameWithoutExtention];
            }

            return reports
                .OrderBy(r => String.IsNullOrEmpty(r.ServerSubFolder))
                .ThenBy(r => r.ServerSubFolder)
                .ThenBy(r => r.FileName);
        }

        private DeployConfigurationModel ParseToConfiguration(Entities.Configuration.Environment environment, String path, IEnumerable<RtReport> reportsFileList)
        {
            return new DeployConfigurationModel
            {
                Name = environment.Name,
                ServerReportFolder = environment.ServerReportFolder,
                ServerDataSetFolder = environment.ServerDataSetFolder,
                ServerDataSourceFolder = environment.ServerDataSourceFolder,
                ServerServiceUrl = environment.ServerUrl,
                SourceReportFolder = path,
                SourceSrc = Constants.LOCAL,
                UploadMode = String.IsNullOrEmpty(environment.UploadMode) || !GetUploadMode.ContainsKey(environment.UploadMode) ? GetUploadMode[String.Empty] : GetUploadMode[environment.UploadMode],
                UploadSuffix = String.Empty,
                Reports = reportsFileList,
                SelectedByDefault = environment.Default
            };
        }

        #endregion
    }
}
