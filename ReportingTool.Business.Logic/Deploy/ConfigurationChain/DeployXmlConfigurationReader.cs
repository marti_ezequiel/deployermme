﻿using System;
using System.Collections.Generic;
using System.Linq;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Entities.Exceptions;
using ReportingTool.Business.Interfaces.Logic;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Logic
{
    public class DeployXmlConfigurationReader : DeployConfigurationReaderHandler, IDeployXmlConfigurationReader
    {
        public override ConfigurationSource Configuration
        {
            get
            {
                return ConfigurationSource.XmlFile;
            }
        }

        private readonly IXmlFileSerializer xmlFileSerializer;

        public DeployXmlConfigurationReader(IXmlFileSerializer xmlFileSerializer)
        {
            this.xmlFileSerializer = xmlFileSerializer;
        }

        protected override Boolean IsValid(String path)
        {
            return path.TrimEnd().EndsWith(Labels.DotXml, StringComparison.InvariantCultureIgnoreCase);
        }

        protected override IEnumerable<DeployConfigurationModel> Resolve(String path)
        {
            var configurationXml = this.xmlFileSerializer.Read<ConfigurationXmlFile>(path);

            var validationResults = new ValidationResults();

            configurationXml.Validate(validationResults);

            if (!validationResults.IsValid()) validationResults.ThrowException();

            configurationXml.SetParentsReferences();

            return configurationXml
                .Environments
                .SelectMany(e => e.Groups)
                .Select(e => this.ParseToConfiguration(e));
        }

        private DeployConfigurationModel ParseToConfiguration(GroupXml xml)
        {
            String name = String.IsNullOrEmpty(xml.Name) ? xml.Parent.Name :
                    String.Concat(xml.Parent.Name, Labels.Separator, xml.Name);

            return new DeployConfigurationModel
            {
                Name = name,
                ServerReportFolder = xml.Config.Server.ReportFolder,
                ServerDataSetFolder = xml.Config.Server.DataSetFolder,
                ServerDataSourceFolder = xml.Config.Server.DataSourceFolder,
                ServerServiceUrl = xml.Config.Server.Url,
                SourceReportFolder = xml.Config.Source.ReportFolder,
                SourceSrc = xml.Config.Source.Src,
                UploadMode = this.GetUploadMode[xml.Parent.UploadMode],
                UploadSuffix = xml.Parent.UploadSuffix,
                Reports = xml.Reports.Select(r => new RtReport
                {
                    Name = r.Name,
                    FullLocalPath = String.Format(Masks.XSlashY, xml.Config.Source.ReportFolder, r.FileName),
                    Params = r.Params.ToDictionary(p => p.Key, p => p.Value)
                })
            };
        }
    }
}

