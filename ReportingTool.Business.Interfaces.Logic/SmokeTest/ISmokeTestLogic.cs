﻿using System.Collections.Generic;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Entities.Deploy;
using ReportingTool.Business.Entities.SmokeTest;

namespace ReportingTool.Business.Interfaces.Logic
{
    public interface ISmokeTestLogic
    {
        IEnumerable<RdlParameter> GetParameters(IEnumerable<RtReport> reports);

        void SmokeTest(IEnumerable<RtReport> reports, IEnumerable<RdlParameter> parameters, DeployConfigurationModel model, CredentialEntity credentials);
    }
}
