﻿using System;
using System.Collections.Generic;
using ReportingTool.Business.Entities.SmokeTest;

namespace ReportingTool.Business.Interfaces.Logic
{
    public interface IReportsParameterReader
    {
        IEnumerable<RdlParameter> GetParameters(String reportPath);
    }
}
