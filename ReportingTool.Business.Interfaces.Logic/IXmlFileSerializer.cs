﻿using System;

namespace ReportingTool.Business.Interfaces.Logic
{
    public interface IXmlFileSerializer
    {
        T Read<T>(String path) where T : class;
    }
}
