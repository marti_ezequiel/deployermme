﻿using ReportingTool.Business.Entities;

namespace ReportingTool.Business.Interfaces.Logic
{
    public interface IConfigurationLogic
    {
        CredentialEntity GetCredentials();

        void SetCredentials(CredentialEntity credentials);

        void ClearCredentials();
    }
}
