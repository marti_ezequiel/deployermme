﻿using System;
using System.Collections.Generic;
using ReportingTool.Business.Entities.Deploy;

namespace ReportingTool.Business.Interfaces.Logic
{
    public interface IDeployConfigurationLogic
    {
        IEnumerable<DeployConfigurationModel> GetConfiguration(String path, ref ConfigurationSource resolver);
    }
}
