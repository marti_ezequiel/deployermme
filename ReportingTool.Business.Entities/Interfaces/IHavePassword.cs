﻿using System.Security;

namespace ReportingTool.Business.Entities
{
    public interface IHavePassword
    {
        SecureString Password { get; }
    }
}
