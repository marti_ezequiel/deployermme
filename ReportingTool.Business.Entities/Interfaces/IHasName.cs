﻿using System;

namespace ReportingTool.Business.Entities
{
    public interface IHasName
    {
        String Name { get; }
    }
}
