﻿using System;
using System.Xml.Serialization;

namespace ReportingTool.Business.Entities
{
    public class Entity : IHasName
    {
        public static String NamePropertyName
        {
            get
            {
                return "Name";
            }
        }

        public virtual String Name { get; set; }

        public Entity() { }

        public Entity(String name) : this()
        {
            this.Name = name;
        }
    }
}
