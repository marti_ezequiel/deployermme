﻿using System;
using System.Net;
using System.Runtime.InteropServices;
using System.Security;
using ReportingTool.Exceptions;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities
{
    public class CredentialEntity : Entity, IHavePassword
    {
        #region Attributes

        private String user;

        private SecureString password;

        private String domain;

        private NetworkCredential networkCredential;

        #endregion

        #region Properties

        public String User
        {
            get { return this.user; }
        }

        public String FullUserName
        {
            get
            {
                return String.Concat(this.domain, "/", this.user);
            }
        }

        public SecureString Password
        {
            get { return this.password; }
        }

        public String Domain
        {
            get { return this.domain; }
        }

        public NetworkCredential NetworkCredential
        {
            get
            {
                if (this.IsEmpty()) throw new CredentialException(Messages.UserNamePasswordOrUserDomainIsEmpty);

                if (!this.IsValid()) this.networkCredential = new NetworkCredential(this.User, this.Password, this.Domain);

                return this.networkCredential;
            }
        }

        #endregion

        #region Constructor

        public CredentialEntity(String user, SecureString password, String domain)
        {
            this.user = user;

            this.password = password;

            this.domain = domain;
        }

        #endregion

        #region Methods

        private Boolean IsEmpty()
        {
            return
                String.IsNullOrWhiteSpace(this.User) ||
                this.Password.Length.Equals(0) ||
                String.IsNullOrWhiteSpace(this.Domain);
        }

        private Boolean IsValid()
        {
            return
                this.networkCredential != null &&
                this.networkCredential.UserName.Equals(this.User) &&
                this.networkCredential.Domain.Equals(this.Domain);
        }

        #endregion
    }
}
