﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ReportingTool.Business.Entities.Exceptions
{
    public class ValidationResults
    {
        private IList<String> List { get; set; }

        public IEnumerable<String> ValidationErrors
        {
            get
            {
                return this.List;
            }
        }

        public ValidationResults()
        {
            this.List = new List<String>();
        }

        public void AddValidation(String message)
        {
            this.List.Add(message);
        }

        public Boolean IsValid()
        {
            return !this.List.Any();
        }

        public void ThrowException()
        {
            throw new ValidationException(String.Join(Environment.NewLine, this.List));
        }
    }
}
