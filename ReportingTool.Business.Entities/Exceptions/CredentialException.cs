﻿using System;

namespace ReportingTool.Exceptions
{
    public class CredentialException : Exception
    {
        public CredentialException()
        {
        }

        public CredentialException(String message) : base(message)
        {
        }

        public CredentialException(String message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
