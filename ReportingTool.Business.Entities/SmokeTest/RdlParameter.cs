﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace ReportingTool.Business.Entities.SmokeTest
{
    public class RdlParameter : Entity, INotifyPropertyChanged
    {
        public String ReportFileName { get; set; }

        public Type DataType { get; set; }

        private Object value;
        public Object Value
        {
            get
            {
                return this.value;
            }
            set
            {
                if (value != this.value)
                {
                    this.value = value;

                    if (this.OnValueChange != null) OnValueChange(this, value);
                    if (this.PropertyChanged != null) this.PropertyChanged(this, new PropertyChangedEventArgs("Value"));
                }
            }
        }
        
        public String DataTypeName 
        {
            get
            {
                return this.DataType.ToString().Split('.').Last();
            }
        }

        public RdlParameter()
        {

        }

        public RdlParameter(String name, String reportName, String dataType, String defaultValue)
        {
            this.Name = name;

            this.ReportFileName = reportName;

            this.DataType = Types.ContainsKey(dataType) ?
                Types[dataType] :
                Type.GetType(dataType) ?? Type.GetType("System." + dataType);

            if (this.DataType != null && !String.IsNullOrEmpty(defaultValue) && !defaultValue.StartsWith("="))
            {
                try
                {
                    this.Value = Convert.ChangeType(defaultValue, this.DataType);
                }
                catch
                {
                    this.Value = null;
                }
            }
        }

        private static readonly Dictionary<String, Type> Types = new Dictionary<String, Type>()
        {
            { "String", typeof(String) },
            { "Integer", typeof(Int32) },
            { "DateTime", typeof(DateTime) },
            { "Boolean", typeof(Boolean) },
            { "Float", typeof(Single) }
        };

        public delegate void RdlParameterValueChange(RdlParameter sender, Object newValue);
        
        public event RdlParameterValueChange OnValueChange;
        public event PropertyChangedEventHandler PropertyChanged;
    }
}