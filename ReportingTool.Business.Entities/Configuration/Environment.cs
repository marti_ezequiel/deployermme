﻿using System;

namespace ReportingTool.Business.Entities.Configuration
{
    public class Environment
    {
        public String Name { get; set; }

        public String ServerUrl { get; set; }

        public String ServerReportFolder { get; set; }

        public String ServerDataSourceFolder { get; set; }

        public String ServerDataSetFolder { get; set; }

        public String UploadMode { get; set; }

        public Boolean Default { get; set; }
    }
}
