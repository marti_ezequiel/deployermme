﻿using System;
using System.Xml.Serialization;
using ReportingTool.Business.Entities.Exceptions;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class SourceXml
    {
        [XmlAttribute(AttributeName = "Src")]
        public String Src { get; set; }

        [XmlAttribute(AttributeName = "ReportFolder")]
        public String ReportFolder { get; set; }

        public void Validate(String name, ValidationResults validationResults)
        {
            if (String.IsNullOrEmpty(this.Src)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, String.Concat(name, Labels.Separator, Labels.Source), Labels.Src));

            if (String.IsNullOrEmpty(this.ReportFolder)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, String.Concat(name, Labels.Separator, Labels.Source), Labels.ReportFolder));
        }
    }
}