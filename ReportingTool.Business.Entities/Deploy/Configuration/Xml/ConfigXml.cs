﻿using System;
using System.Xml.Serialization;
using ReportingTool.Business.Entities.Exceptions;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class ConfigXml
    {
        [XmlElement(ElementName = "Server")]
        public ServerXml Server { get; set; }

        [XmlElement(ElementName = "Source")]
        public SourceXml Source { get; set; }

        public void Validate(String name, ValidationResults validationResults)
        {
            if (this.Server == null)
            {
                validationResults.AddValidation(String.Format(Messages.XMustHaveY, name, Labels.Server));
            }
            else
            {
                this.Server.Validate(name, validationResults);
            }

            if (this.Source == null)
            {
                validationResults.AddValidation(String.Format(Messages.XMustHaveY, name, Labels.Source));
            }
            else
            {
                this.Source.Validate(name, validationResults);
            }
        }
    }
}
