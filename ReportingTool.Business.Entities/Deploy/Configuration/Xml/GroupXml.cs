﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using ReportingTool.Business.Entities.Exceptions;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class GroupXml : IHasName
    {
        [XmlAttribute(AttributeName = "Name")]
        public String Name { get; set; }

        [XmlElement(ElementName = "Config")]
        public ConfigXml Config { get; set; }

        [XmlArrayItem(ElementName = "Report")]
        public List<ReportXml> Reports { get; set; }

        [XmlIgnore]
        public EnvironmentXml Parent { get; private set; }

        public void Validate(ValidationResults validationResults)
        {
            String name = String.IsNullOrWhiteSpace(this.Name) ? Labels.Group : this.Name;

            if (String.IsNullOrWhiteSpace(this.Name)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, name, Labels.Name));

            if (this.Config == null)
            {
                validationResults.AddValidation(String.Format(Messages.XMustHaveY, name, Labels.Config));
            }
            else
            {
                this.Config.Validate(name, validationResults);
            }

            if (this.Reports == null || !this.Reports.Any())
            {
                validationResults.AddValidation(String.Format(Messages.XMustHaveAtLeastOneY, name, Labels.Report));

                return;
            }

            foreach (ReportXml report in this.Reports)
            {
                report.Validate(validationResults);
            }
        }

        public void SetParentsReferences(EnvironmentXml environmentXml)
        {
            this.Parent = environmentXml;

            foreach (ReportXml report in this.Reports) report.SetParentsReferences(this);
        }
    }
}
