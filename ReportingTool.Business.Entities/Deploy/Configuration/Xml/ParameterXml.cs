﻿using System;
using System.Xml.Serialization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class ParameterXml
    {
        [XmlAttribute(AttributeName = "Name")]
        public String Key { get; set; }

        [XmlAttribute(AttributeName = "Value")]
        public String Value { get; set; }
    }
}
