﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using ReportingTool.Business.Entities.Exceptions;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class EnvironmentXml : IHasName
    {
        [XmlAttribute(AttributeName = "Name")]
        public String Name { get; set; }

        [XmlAttribute(AttributeName = "UploadMode")]
        public String UploadMode { get; set; }

        [XmlAttribute(AttributeName = "UploadSuffix")]
        public String UploadSuffix { get; set; }

        [XmlElement(ElementName = "Group")]
        public List<GroupXml> Groups { get; set; }

        [XmlIgnore]
        public ConfigurationXmlFile Parent { get; private set; }

        public void Validate(ValidationResults validationResults)
        {
            String name = String.IsNullOrWhiteSpace(this.Name) ? Labels.Environment : this.Name;

            if (String.IsNullOrWhiteSpace(this.Name)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, name, Labels.Name));

            if (!this.ValidsUploadModes.Contains(this.UploadMode)) validationResults.AddValidation(String.Format(Messages.InvalidXForY, Labels.UploadMode));

            else if (this.UploadModesWithSuffix.Contains(this.UploadMode) && String.IsNullOrWhiteSpace(this.UploadSuffix)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, name, Labels.UploadSuffix));
            
            if (this.Groups == null || !this.Groups.Any())
            {
                validationResults.AddValidation(String.Format(Messages.XMustHaveAtLeastOneY, name, Labels.Group));

                return;
            }
            
            foreach (GroupXml group in this.Groups)
            {
                group.Validate(validationResults);
            }
        }

        public void SetParentsReferences(ConfigurationXmlFile xmlConfigurationFile)
        {
            this.Parent = xmlConfigurationFile;

            foreach (GroupXml group in this.Groups) group.SetParentsReferences(this);
        }

        private readonly List<String> ValidsUploadModes = new List<String>()
        {
            String.Empty,
            Labels.OverWrite,
            Labels.DeleteExisting,
            Labels.RenameExisting,
            Labels.RenameNew
        };

        private readonly List<String> UploadModesWithSuffix = new List<String>()
        {
            Labels.RenameExisting,
            Labels.RenameNew
        };
    }
}
