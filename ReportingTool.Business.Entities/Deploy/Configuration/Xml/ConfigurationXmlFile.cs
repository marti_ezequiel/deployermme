﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using ReportingTool.Business.Entities.Exceptions;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    [XmlRoot(ElementName = "ConfigFile")]
    public class ConfigurationXmlFile
    {
        [XmlElement(ElementName = "Environment")]
        public List<EnvironmentXml> Environments { get; set; }

        public void Validate(ValidationResults validationResults)
        {
            if (this.Environments == null || !this.Environments.Any())
            {
                validationResults.AddValidation(String.Format(Messages.XMustHaveAtLeastOneY, Labels.ConfigurationFile, Labels.Environment));

                return;
            }

            foreach(EnvironmentXml environment in this.Environments)
            {
                environment.Validate(validationResults);
            }
        }

        public void SetParentsReferences()
        {
            foreach (EnvironmentXml environment in this.Environments) environment.SetParentsReferences(this);
        }
    }
}
