﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using ReportingTool.Business.Entities.Exceptions;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class ReportXml
    {
        [XmlAttribute(AttributeName = "FileName")]
        public String FileName { get; set; }

        [XmlAttribute(AttributeName = "Name")]
        public String Name { get; set; }

        [XmlArrayItem(ElementName = "Param")]
        public List<ParameterXml> Params { get; set; }
        
        [XmlIgnore]
        public GroupXml Parent { get; private set; }

        public void Validate(ValidationResults validationResults)
        {
            String name = String.IsNullOrWhiteSpace(this.Name) ? Labels.Report : this.Name;

            if (String.IsNullOrWhiteSpace(this.Name)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, name, Labels.Name));
            
            if (String.IsNullOrWhiteSpace(this.FileName)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, name, Labels.FileName));

            foreach(var param in this.Params.GroupBy(p => p.Key).Where(p => p.Count() > 1))
            {
                validationResults.AddValidation(String.Format(Messages.ReportXHasMoreThanOneYParameter, name, param.Key));
            }
        }

        internal void SetParentsReferences(GroupXml groupXml)
        {
            this.Parent = groupXml;
        }
    }
}