﻿using System;
using System.Xml.Serialization;
using ReportingTool.Business.Entities.Exceptions;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class ServerXml
    {
        [XmlAttribute(AttributeName = "Url")]
        public String Url { get; set; }

        [XmlAttribute(AttributeName = "ReportFolder")]
        public String ReportFolder { get; set; }

        [XmlAttribute(AttributeName = "DataSourceFolder")]
        public String DataSourceFolder { get; set; }

        [XmlAttribute(AttributeName = "DataSetFolder")]
        public String DataSetFolder { get; set; }

        public void Validate(String name, ValidationResults validationResults)
        {
            if (String.IsNullOrEmpty(this.Url)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, String.Concat(name, Labels.Separator, Labels.Server), Labels.Url));

            if (String.IsNullOrEmpty(this.ReportFolder)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, String.Concat(name, Labels.Separator, Labels.Server), Labels.ReportFolder));

            if (String.IsNullOrEmpty(this.DataSourceFolder)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, String.Concat(name, Labels.Separator, Labels.Server), Labels.DataSourceFolder));

            if (String.IsNullOrEmpty(this.DataSetFolder)) validationResults.AddValidation(String.Format(Messages.XMustHaveY, String.Concat(name, Labels.Separator, Labels.Server), Labels.DataSetFolder));
        }
    }
}
