﻿using System;
using System.Collections.Generic;
using AccentureCIO;
using ReportingTool.Resources.Parameters;

namespace ReportingTool.Business.Entities.Deploy
{
    public class DeployConfigurationModel : IHasName
    {
        #region Attributes

        private UploadReportConfig uploadReportConfig;

        #endregion

        #region Properties

        public String ServerReportFolder { get; set; }

        public String ServerDataSetFolder { get; set; }

        public String ServerDataSourceFolder { get; set; }

        public String ServerServiceUrl { get; set; }

        public String SourceReportFolder { get; set; }

        public String SourceSrc { get; set; }

        public IEnumerable<RtReport> Reports { get; set; }

        public UploadMode UploadMode { get; set; }

        public String UploadSuffix { get; set; }

        public String Name { get; set; }

        public Boolean SelectedByDefault { get; set; }

        #endregion

        #region Custom Properties

        public Boolean IsProdServerOrStageServer
        {
            get
            {
                return
                    this.ServerServiceUrl.Equals(Constants.PROD_SERVER, StringComparison.InvariantCultureIgnoreCase) ||
                    this.ServerServiceUrl.Equals(Constants.STAGE_SERVER, StringComparison.InvariantCultureIgnoreCase);
            }
        }

        public Boolean IsLocalSource
        {
            get
            {
                return this.SourceSrc.Equals(Constants.LOCAL, StringComparison.CurrentCultureIgnoreCase);
            }
        }

        public UploadReportConfig UploadReportConfig
        {
            get
            {
                if (this.uploadReportConfig == null) this.DefineUploadReportConfig();

                return this.uploadReportConfig;
            }
        }

        public String ServerExecutionUrl
        {
            get
            {
                return String.Concat(
                    this.ServerServiceUrl.Substring(0, this.ServerServiceUrl.LastIndexOf('/') + 1),
                    "ReportExecution2005.asmx");
            }
        }

        #endregion

        #region Constructor

        public DeployConfigurationModel()
        {
            this.Reports = new List<RtReport>();
        }

        #endregion

        #region Methods

        private void DefineUploadReportConfig()
        {
            this.uploadReportConfig = new UploadReportConfig();

            switch (this.UploadMode)
            {
                case UploadMode.OverWrite:
                    this.uploadReportConfig.SetOverWrite();
                    break;

                case UploadMode.DeleteExisting:
                    this.uploadReportConfig.SetDeleteExisting();
                    break;

                case UploadMode.RenameExisting:
                    this.UploadReportConfig.SetRenameExisting(this.UploadSuffix);
                    break;

                case UploadMode.RenameNew:
                    this.UploadReportConfig.SetRenameNew(this.UploadSuffix);
                    break;
            }
        }

        #endregion
    }
}
