﻿using System;
using System.Collections.Generic;
using AccentureCIO;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class RtReport : Report, IHasName
    { 
        #region Properties
        
        public String ReportAlias { get; set; }
        
        public Dictionary<String, String> Params { get; set; }

        public String ServerSubFolder { get; set; }

        public String FileName { get; set; }
        
        #endregion

        #region Custom Properties

        public String FullLocalPath
        {
            get
            {
                return String.Format(Masks.XInvertedSlashY, this.Path, this.FileName);
            }
            set
            {
                this.FileName = System.IO.Path.GetFileName(value);

                this.Path = System.IO.Path.GetDirectoryName(value);
            }
        }

        public String FileNameWithoutExtention
        {
            get
            {
                return System.IO.Path.GetFileNameWithoutExtension(this.FileName);
            }
        }

        public new String Name 
        {
            get
            {
                return String.IsNullOrEmpty(base.Name) ? 
                    String.IsNullOrEmpty(this.ServerSubFolder) ? this.FileName : String.Format(Masks.XSlashY, this.ServerSubFolder, this.FileName) : 
                    base.Name;
            }
            set
            {
                base.Name = value;
            }
        }

        #endregion

        #region Constructors

        public RtReport()
        {
            this.Params = new Dictionary<String, String>();
        }

        public RtReport(String name, String path, List<InternalDataset> internalDatasets, List<SharedDataset> sharedDatasets, List<ReportParameter> reportParameters, List<DataSource> dataSources)
            : base(name, path, internalDatasets, sharedDatasets, reportParameters, dataSources)
        {
        }

        public RtReport(Report report, String alias)
        {
            this.DataSources = report.DataSources;

            this.ErrorMsg = report.ErrorMsg;

            this.HasErrors = report.HasErrors;

            this.InternalDatasets = report.InternalDatasets;

            this.Name = report.Name;

            this.Path = report.Path;

            this.ReportAlias = alias;

            this.ReportParameters = report.ReportParameters;

            this.SharedDatasets = report.SharedDatasets;
        }

        #endregion

        #region Methods

        #endregion
    }
}
