﻿namespace ReportingTool.Business.Entities.Deploy
{
    public enum UploadMode
    {
        OverWrite,
        DeleteExisting,
        RenameExisting,
        RenameNew
    }
}
