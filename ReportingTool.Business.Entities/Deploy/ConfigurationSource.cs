﻿namespace ReportingTool.Business.Entities.Deploy
{
    public enum ConfigurationSource
    {
        None,
        XmlFile,
        Folder,
        TeamFoundationServer
    }
}
