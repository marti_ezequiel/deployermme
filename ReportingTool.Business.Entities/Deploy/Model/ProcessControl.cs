﻿using System;
using ReportingTool.Resources.Globalization;

namespace ReportingTool.Business.Entities.Deploy
{
    public class ProcessControl
    {
        #region Properties

        public DateTime? StartTime { get; private set; }

        public DateTime? EndTime { get; private set; }

        public String Name { get; private set; }

        #endregion

        public ProcessControl(String name)
        {
            this.Name = name;
        }

        public void Start()
        {
            this.StartTime = DateTime.Now;
        }

        public void Stop()
        {
            this.EndTime = DateTime.Now;
        }

        public override String ToString()
        {
            return this.Name;
        }

        public String GetEnlapsedTime()
        {
            if (this.StartTime.HasValue && this.EndTime.HasValue && this.StartTime != this.EndTime)
            {
                TimeSpan enlapsedTime = this.EndTime.Value - this.StartTime.Value;

                return String.Concat(
                    enlapsedTime.TotalDays >= 1 ? String.Format(Masks.Xd, enlapsedTime.Days) : String.Empty,
                    enlapsedTime.TotalHours >= 1 ? String.Format(Masks.Xhs, enlapsedTime.Hours) : String.Empty,
                    enlapsedTime.TotalMinutes >= 1 ? String.Format(Masks.Xmin, enlapsedTime.Minutes) : String.Empty,
                    String.Format(Masks.Xsec, enlapsedTime.Seconds)
                    );
            }

            return String.Empty;
        }
    }
}
