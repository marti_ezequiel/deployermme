﻿using System;

namespace ReportingTool.Business.Entities.Deploy
{
    public class DataSourceStatus
    {
        public DataSourceStatus(String source, RtReport report, String ds, String reference, Boolean isSuccess)
        {
            this.Source = source;
            this.Report = report;
            this.Ds = ds;
            this.Reference = reference;
            this.IsSuccess = isSuccess;
        }

        public String Source { get; set; }

        public RtReport Report { get; set; }

        public String Ds { get; set; }

        public String Reference { get; set; }

        public Boolean IsSuccess { get; set; }
    }
}
