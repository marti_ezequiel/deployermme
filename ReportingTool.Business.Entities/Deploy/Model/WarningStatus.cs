﻿using System;

namespace ReportingTool.Business.Entities.Deploy
{
    public class WarningStatus
    {
        public WarningStatus(RtReport report, String message)
        {
            this.Report = report;
            this.Message = message;
        }

        public RtReport Report { get; set; }

        public String Message { get; set; }
    }
}
