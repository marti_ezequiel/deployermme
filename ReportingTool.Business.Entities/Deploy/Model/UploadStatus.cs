﻿using System;

namespace ReportingTool.Business.Entities.Deploy
{
    public class UploadStatus
    {
        public UploadStatus(RtReport report, Boolean isSucces)
        {
            this.Report = report;
            this.IsSuccess = isSucces;
        }

        public RtReport Report { get; set; }

        public Boolean IsSuccess { get; set; }
    }
}
