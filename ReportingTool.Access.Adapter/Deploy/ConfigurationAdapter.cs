﻿using ReportingTool.Access.Adapter.Interfaces;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Entities.Deploy;

namespace ReportingTool.Access.Adapter
{
    public class ConfigurationAdapter : IConfigurationAdapter
    {
        public CredentialEntity GetCredentials()
        {
            return DataBase.Instance.Credentials;
        }

        public void SetCredentials(CredentialEntity credentials)
        {
            DataBase.Instance.Credentials = credentials;
        }

        public void ClearCredentials()
        {
            DataBase.Instance.Credentials = null;
        }
    }
}
