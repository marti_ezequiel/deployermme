﻿using System;
using System.Linq;
using System.ServiceModel;
using ReportingTool.Access.Adapter.Interfaces;
using ReportingTool.Access.ExternalServices.ReportExecution2005Service;
using ReportingTool.Access.ExternalServices.ReportingService2010;
using ReportingTool.Business.Entities;
using ReportingTool.Business.Entities.Deploy;
using Utilities = ReportingUtilitiesLib.ReportingService2010WebReference;
using ReportingUtilitiesLib.Classes.UtilityEntities;
using Report = ReportingTool.Access.ExternalServices.ReportExecution2005Service;
using Reporting = ReportingTool.Access.ExternalServices.ReportingService2010;
using AccentureCIO;
using ReportingTool.Resources.Globalization;
using ReportingTool.Business.Entities.Exceptions;
using System.Runtime.InteropServices;
using System.Security;
using ReportingTool.Exceptions;
using System.Collections.Generic;

namespace ReportingTool.Access.Adapter
{
    public class DeployAdapter : IDeployAdapter
    {
        public void CheckCredentials(CredentialEntity credential)
        {
            throw new NotImplementedException();
        }

        public Boolean RenderReport(RtReport report, DeployConfigurationModel group, CredentialEntity credential)
        {
            try
            {
                Byte[] result = null;

                String serverReportFolder =
                    String.IsNullOrEmpty(report.ServerSubFolder) ?
                    group.ServerReportFolder :
                    String.Format(Masks.XSlashY, group.ServerReportFolder, report.ServerSubFolder);

                String reportFullPath =
                    String.Format(Masks.XSlashY, serverReportFolder, report.FileNameWithoutExtention);

                BasicHttpBinding basicHttpBinding = this.CreateBasicHttpBinding();

                using (ReportingService2010SoapClient rsService =
                    new ReportingService2010SoapClient(basicHttpBinding, new EndpointAddress(group.ServerServiceUrl)))
                {
                    Reporting.TrustedUserHeader trusteduserHeader = new Reporting.TrustedUserHeader() { UserName = credential.FullUserName };
                    Report.TrustedUserHeader userHeader = new Report.TrustedUserHeader() { UserName = credential.FullUserName };

                    this.GetHeaders(credential, rsService);

                    SearchCondition[] conditions = this.GetConditions(report);

                    rsService.FindItems(trusteduserHeader, serverReportFolder, BooleanOperatorEnum.And, new Property[0], conditions, out CatalogItem[] foundItems);

                    CatalogItem item =
                        (foundItems ?? new CatalogItem[0])
                        .FirstOrDefault(fi => fi.Path.Equals(reportFullPath));

                    if (item != null)
                    {
                        String format = "EXCEL";
                        String devInfo = "<DeviceInfo><Toolbar>False</Toolbar></DeviceInfo>";

                        Report.ServerInfoHeader execServerHeader = new Report.ServerInfoHeader();
                        Report.ExecutionInfo execInfo = new ExecutionInfo();
                        Report.ParameterValue[] Parameters = report.Params.Select(p => new Report.ParameterValue()
                        {
                            Name = p.Key,
                            Value = p.Value
                        }).ToArray();

                        ReportExecutionServiceSoapClient rsExec = this.GetReportExecutionServiceSoapClient(credential, group.ServerExecutionUrl, basicHttpBinding);

                        rsExec.LoadReport(userHeader, item.Path, null, out execServerHeader, out execInfo);

                        Report.ExecutionHeader execHeader = new ExecutionHeader() { ExecutionID = execInfo.ExecutionID };

                        rsExec.SetExecutionParameters(execHeader, userHeader, Parameters, "en-us", out execInfo);

                        rsExec.Render(execHeader, userHeader, format, devInfo, out result, out String extension, out String mimeType, out String encoding, out Report.Warning[] warnings, out String[] streamIDs);
                    }
                }

                return result != null;
            }
            catch (System.ServiceModel.Security.MessageSecurityException mse)
            {
                if (mse.Message.ToLower().Contains(Labels.Unauthorized)) throw new CredentialException(Messages.UserWithoutPermissions);

                throw mse;
            }
        }

        public IEnumerable<RtReport> UploadReport(DeployConfigurationModel group, IEnumerable<RtReport> reports, CredentialEntity credential, IProgress<UploadStatus> logUpload, IProgress<WarningStatus> logWarning, IProgress<DataSourceStatus> logDataSource)
        {
            var ssrs = new Utilities.ReportingService2010()
            {
                Credentials = credential.NetworkCredential,
                Url = group.ServerServiceUrl
            };

            List<RtReport> reportsReadyForSmokeTest = new List<RtReport>();

            if (group.IsLocalSource)
            {
                foreach (RtReport report in reports)
                {
                    Boolean ignoreForSmokeTest = false;
                    
                    String serverReportFolder =
                        String.IsNullOrEmpty(report.ServerSubFolder) ?
                        group.ServerReportFolder :
                        String.Format(Masks.XSlashY, group.ServerReportFolder, report.ServerSubFolder);

                    String reportFullPath =
                        String.Format(Masks.XSlashY, serverReportFolder, report.FileNameWithoutExtention);

                    try
                    {
                        var reportingUtilities = new ReportingUtilities(ssrs);

                        var warnings = reportingUtilities.UploadReport(report.FullLocalPath, serverReportFolder, report.FileNameWithoutExtention, group.UploadReportConfig);

                        logUpload.Report(new UploadStatus(report, true));

                        foreach (var warning in warnings) logWarning.Report(new WarningStatus(report, warning.Message));

                        var reportStream = new RtReport(new LocalReportingUtilities().GetReportFromDisk(report.FullLocalPath), String.Empty);

                        foreach(var ds in reportStream.DataSources)
                        {
                            String reference = String.Format(Masks.XSlashY, group.ServerDataSourceFolder, ds.Reference);

                            try
                            {
                                reportingUtilities.UpdateReportDataSource(reportFullPath, ds.Name, reference);

                                logDataSource.Report(new DataSourceStatus(Labels.Datasource, report, ds.Name, reference, true));
                            }
                            catch (System.Web.Services.Protocols.SoapException se)
                            {
                                if (se.Message.Contains(Labels.Permission)) throw new CredentialException(Messages.UserWithoutPermissions);

                                logDataSource.Report(new DataSourceStatus(Labels.Datasource, report, ds.Name, reference, false));

                                ignoreForSmokeTest = true;
                            }
                            catch
                            {
                                logDataSource.Report(new DataSourceStatus(Labels.Datasource, report, ds.Name, reference, false));

                                ignoreForSmokeTest = true;
                            }
                        }

                        foreach(var ds in reportStream.SharedDatasets)
                        {
                            String reference = String.Format(Masks.XSlashY, group.ServerDataSetFolder, ds.Reference);

                            try
                            {
                                reportingUtilities.UpdateReportDataSet(reportFullPath, ds.Name, reference);

                                logDataSource.Report(new DataSourceStatus(Labels.SharedDataset, report, ds.Name, reference, true));
                            }
                            catch (System.Web.Services.Protocols.SoapException se)
                            {
                                if (se.Message.Contains(Labels.Permission)) throw new CredentialException(Messages.UserWithoutPermissions);
                                
                                logDataSource.Report(new DataSourceStatus(Labels.SharedDataset, report, ds.Name, reference, false));

                                ignoreForSmokeTest = true;
                            }
                            catch
                            {
                                logDataSource.Report(new DataSourceStatus(Labels.SharedDataset, report, ds.Name, reference, false));

                                ignoreForSmokeTest = true;
                            }
                        }
                    }
                    catch (System.Net.WebException we)
                    {
                        if (we.Message.ToLower().Contains(Labels.Unauthorized)) throw new CredentialException(Messages.UserWithoutPermissions);

                        logUpload.Report(new UploadStatus(report, false));

                        throw;
                    }
                    catch (CredentialException)
                    {
                        throw;
                    }
                    catch (Exception)
                    {
                        logUpload.Report(new UploadStatus(report, false));

                        throw;
                    }

                    if (!ignoreForSmokeTest) reportsReadyForSmokeTest.Add(report);
                }
            }

            return reportsReadyForSmokeTest;
        }
                
        private BasicHttpBinding CreateBasicHttpBinding()
        {
            var basicHttpBinding = new BasicHttpBinding(BasicHttpSecurityMode.Transport)
            {
                TransferMode = TransferMode.Buffered,
                MaxReceivedMessageSize = 2147483646L,
                SendTimeout = TimeSpan.MaxValue
            };

            basicHttpBinding.Security.Mode = BasicHttpSecurityMode.Transport;
            basicHttpBinding.Security.Transport.ProxyCredentialType = HttpProxyCredentialType.None;
            basicHttpBinding.Security.Transport.Realm = String.Empty;
            basicHttpBinding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Ntlm;
            basicHttpBinding.Security.Message.ClientCredentialType = BasicHttpMessageCredentialType.Certificate;

            return basicHttpBinding;
        }

        private void GetHeaders(CredentialEntity credential, ReportingService2010SoapClient rsService)
        {
            rsService.ClientCredentials.SupportInteractive = false;

            rsService.ClientCredentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;
            rsService.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

            rsService.ClientCredentials.UserName.UserName = credential.FullUserName;
            rsService.ClientCredentials.UserName.Password = this.ConvertToUnsecureString(credential.Password);
        }

        private SearchCondition[] GetConditions(RtReport report)
        {
            return new SearchCondition[]
            {
                new SearchCondition()
                {
                    Condition = ConditionEnum.Equals,
                    ConditionSpecified = true,
                    Name = "Name",
                    Values = new String[] { report.FileNameWithoutExtention }
                }
            };
        }

        private ReportExecutionServiceSoapClient GetReportExecutionServiceSoapClient(CredentialEntity credential, String execUrl, BasicHttpBinding basicHttpBinding)
        {
            ReportExecutionServiceSoapClient rsExec = new ReportExecutionServiceSoapClient(basicHttpBinding, new EndpointAddress(execUrl));

            rsExec.ClientCredentials.Windows.ClientCredential = System.Net.CredentialCache.DefaultNetworkCredentials;
            rsExec.ClientCredentials.Windows.AllowedImpersonationLevel = System.Security.Principal.TokenImpersonationLevel.Impersonation;

            rsExec.ClientCredentials.UserName.UserName = credential.FullUserName;
            rsExec.ClientCredentials.UserName.Password = this.ConvertToUnsecureString(credential.Password);
            
            return rsExec;
        }

        private String ConvertToUnsecureString(SecureString securePassword)
        {
            if (securePassword == null)
            {
                return String.Empty;
            }

            IntPtr unmanagedString = IntPtr.Zero;
            try
            {
                unmanagedString = Marshal.SecureStringToGlobalAllocUnicode(securePassword);
                return Marshal.PtrToStringUni(unmanagedString);
            }
            finally
            {
                Marshal.ZeroFreeGlobalAllocUnicode(unmanagedString);
            }
        }
    }
}
