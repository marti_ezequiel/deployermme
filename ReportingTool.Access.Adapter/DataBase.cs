﻿using ReportingTool.Business.Entities;

namespace ReportingTool.Access.Adapter
{
    internal class DataBase
    {
        internal CredentialEntity Credentials { get; set; }

        private DataBase() 
        {

        }

        private static DataBase dataBase;

        internal static DataBase Instance
        {
            get
            {
                if (dataBase == null) dataBase = new DataBase();

                return dataBase;
            }
        }
    }
}
